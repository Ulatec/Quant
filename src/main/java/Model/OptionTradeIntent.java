package Model;

public class OptionTradeIntent {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RED = "\033[31;1;4m";

    public static final String ANSI_RESET = "\u001B[0m";
    String tradeComment;

    String side;

    String openCLose;

    OptionContract optionContract;
    double vwap;

    public OptionTradeIntent(String tradeComment, String side, String openCLose, OptionContract optionContract, double vwap) {
        this.side = side;
        if(side.equals("long")){
            this.tradeComment = ANSI_GREEN + tradeComment + ANSI_RESET;
        }else if(side.equals("short")){
            this.tradeComment = ANSI_RED + tradeComment + ANSI_RESET;
        }else{
            this.tradeComment = tradeComment;
        }
        this.openCLose = openCLose;
        this.vwap = vwap;
        this.optionContract = optionContract;

    }

    public String getTradeComment() {
        return tradeComment;
    }

    public void setTradeComment(String tradeComment) {
        this.tradeComment = tradeComment;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }

    public String getOpenCLose() {
        return openCLose;
    }

    public void setOpenCLose(String openCLose) {
        this.openCLose = openCLose;
    }

    public double getVwap() {
        return vwap;
    }

    public void setVwap(double vwap) {
        this.vwap = vwap;
    }
}
