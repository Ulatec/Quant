package Threads;

import BackTest.*;
import Libraries.StockCalculationLibrary;
import Model.*;
import Repository.RealizedVolatilityRepository;
import Util.DateFormatter;
import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.h2.result.SimpleResult;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.ObjectInputFilter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;

import static Fetchers.StockRangeTester.*;


public class BackTestThread extends Thread {
    private final int threadNum;
    public boolean log;


    public List<ConfigurationTest> configurationTestList;


    public HashMap<Ticker,HashMap<Integer,List<Bar>>> completedBarCache;

    public List<Bar> completedWithIV;

    public Ticker ticker;
    Instant now;
    Instant start;
    double delta;
    double rate;
    public int complete;
    BackTestThreadMonitor backTestThreadMonitor;
    private RealizedVolatilityRepository realizedVolatilityRepository;
   // long exclusionBegin = Date.from(LocalDate.of(2002,2,15).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();
   // long exclusionEnd = Date.from(LocalDate.of(2002,4,1).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()).getTime();

    LocalDate finalDate;
    private boolean logData = false;
    private boolean multiTicker;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");



    public BackTestThread(List<ConfigurationTest> configurationTestList,
                          List<Bar> completedWithIV,
                           boolean log,
                          BackTestThreadMonitor backTestThreadMonitor, Ticker ticker, int threadNum, RealizedVolatilityRepository realizedVolatilityRepository,
                           boolean multiTicker, LocalDate finalDate
    ){
        this.completedBarCache = new HashMap<>();
        this.configurationTestList = configurationTestList;
        this.completedWithIV = completedWithIV;

        this.log = log;
        this.backTestThreadMonitor = backTestThreadMonitor;
        this.ticker =ticker;
        this.threadNum = threadNum;
        this.realizedVolatilityRepository = realizedVolatilityRepository;
        this.multiTicker = multiTicker;
        this.finalDate = finalDate;
        complete = 0;
    }
    public void run(){
        double[] longOpenVolatilityRow = {0,0,0};
        start = Instant.now();
        StockCalculationLibrary stockCalculationLibrary = new StockCalculationLibrary(realizedVolatilityRepository,null);
        //TrendCalculation trendCalculation = new TrendCalculation();
        //Collections.reverse(completedWithIV);

        // for(Map.Entry<Ticker,List<Bar>> entry : cleanBarCache.entrySet()) {

        int completedSize = completedWithIV.size();
        for (int z = 0; z < completedSize; z++) {
            completedWithIV.get(z).setBaseVolatility(stockCalculationLibrary.getLogVarianceReverse(completedWithIV, z, 30 + 1, ticker.getTicker()));
        completedWithIV.get(z).setBaseLongVolatility(stockCalculationLibrary.getLogVarianceReverse(completedWithIV, z, 90 + 1, ticker.getTicker()));
            setPreviousEarningsDates(completedWithIV,z);
        }
        completedBarCache.put(ticker,new HashMap<>());
        //Collections.reverse(completedWithIV);
        int configSize = configurationTestList.size();
        for (int configurationIndex = 0; configurationIndex<configSize; configurationIndex++ ) {
                ConfigurationTest configurationTest = configurationTestList.get(configurationIndex);
//            Ticker matchedTicker = null;
//            for (Ticker ticker1 : tickers) {
//                if (ticker1.getTicker().equals(ticker)) {
//                    matchedTicker = ticker1;
//                    break;
//                }
//            }

            ArrayList<Bar> barList = new ArrayList<>(completedWithIV.size());
            RegressionCompareObject regressionCompareObject = new RegressionCompareObject(
                    configurationTest.getMovingTrendLength()[1], configurationTest.getRevenueRocWeighting(), configurationTest.getMovingTrendLength()[2],
                    configurationTest.getOilWeighting(), configurationTest.getMovingTrendLength()[0], configurationTest.getCommodityWeighting(),configurationTest.getMovingTrendLength()[5]
            );

            int hashCode = regressionCompareObject.hashCode();
            if (completedBarCache.get(ticker).get(hashCode) != null) {
//                List<Bar> tempBars = completedBarCache.get(hashCode);
//                for (Bar item : tempBars) {
//                    try {
//                        barList.add((Bar) item.clone());
//                    }catch (Exception ignored){
//
//                    }
//                }
                barList = (ArrayList<Bar>) completedBarCache.get(ticker).get(hashCode);

            } else {

                int completedWithIVSize = completedWithIV.size();
                //barList = (ArrayList<Bar>) completedWithIV;
                for (int itemIndex = 0; itemIndex < completedWithIVSize; itemIndex++) {
                    Bar item = completedWithIV.get(itemIndex);
                    try {
                        barList.add((Bar) item.clone());
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                }
                for (int z = 0; z < completedSize; z++) {
                    barList.get(z).setBaseVolatility(stockCalculationLibrary.getLogVarianceReverse(barList, z, (int) (configurationTest.getVolLookback() + 1), ticker.getTicker()));
                    // barList.get(z).setNasdaqVol(stockCalculationLibrary.getNasdaqLogVarianceReverse(nasdaqBars, nasdaqBars.get(z), nasdaqBars.get(z).getDate(), (int) (configurationTest.getVolLookback() + 1), z));
                }
                // Collections.copy(barListSafe, barList);
                Collections.reverse(barList);

                int size = barList.size();




                for (int z = 0; z < size; z++) {
                //    calculateTrendLookback(stockCalculationLibrary, configurationTest, barList, z, false);
                }


//                for (int x = 0; x < size; x++) {
//                    stepOne(stockCalculationLibrary, barList, x, configurationTest);
//                }
//                int lowBetaIndex = 0;
//                for(int x = 0; x < size; x++){
//                    if(barList.get(x).getDate().getTime() == lowBetaBars.get(lowBetaBars.size()-1).getDate().getTime()){
//                        lowBetaIndex = x;
//                        break;
//                    }
//                }

//                for (int x = 0; x < size; x++) {
//                    stepTwo(stockCalculationLibrary, barList, barList.get(x), x, configurationTest);
//                }
//                for (int x = barList.size() - 1; x >= 0; x--) {
//                    stepEight(stockCalculationLibrary, barList, barList.get(x), x, configurationTest);
//                }
//                for (int k = 0; k < size; k++) {
//                    stepThree(stockCalculationLibrary, barList, barList.get(k), k);
//                }
                for (int x = 0; x < size; x++) {
                //   stepFour(stockCalculationLibrary, barList, barList.get(x), x, configurationTest.getCorrelationDays(), true, 0, true);
                    //stepFour(stockCalculationLibrary, barList, barList.get(x), x, configurationTest.getCorrelationDays(), false, lowBetaIndex);
                }

//                for (int k = size - 1; k >= 0; k--) {
//                    //stepSix(configurationTest, matchedTicker, barList, k, false, trendCalculation, stockCalculationLibrary);
//                }
//
//                for (int k = size - 1; k >= 0; k--) {
//                    //stepSeven(configurationTest, matchedTicker, barList, k, false, trendCalculation, stockCalculationLibrary);
//                }


                calculateDaysVolTriggerSlope(barList, (int) configurationTest.getMovingTrendLength()[1], (int) configurationTest.getMovingTrendLength()[2], (int) configurationTest.getMovingTrendLength()[0], true,configurationTest);
                //  ArrayList<Bar> referenceCopy = new ArrayList<>();
//
                getVolumeChange(barList, configurationTest.getTreasuryWeighting(), configurationTest);
                completedBarCache.get(ticker).put(hashCode, barList);
            }

            Collections.reverse(barList);


            double dollars;
            //double optionDollars = 10000;
            if (configurationTest.getDollars() == 0.0) {
                dollars = 100.00;
            } else {
                dollars = configurationTest.getDollars();
            }

            //   double longTradeBasis = 0.0;
            double longsuccess = 0.0;
            double longfail = 0.0;
            double shortsuccess = 0.0;
            double shortfail = 0.0;
            int longconsecutiveTrendBreaks = 0;
            int shortconsecutiveTrendBreaks = 0;
            int longconsecutiveTrendConfirm = 0;
            // int shortconsecutiveTrendConfirm = 0;
            boolean stopLossActive = false;
            // List<String> orderedStrings = new ArrayList<>();
            TradeLog tradeLog = new TradeLog();
            int listSize = barList.size();
            for (int z = 0; z < listSize; z++) {
                if (z > 60) {
                    Bar bar = barList.get(z);

                    boolean longAction = false;
                    boolean shortAction = false;




//  LONG CLOSE //
                        if (tradeLog.isLongActive()) {
                            TradeIntent longTradeIntent = longExitConditions(bar, barList.get(z - 1), configurationTest.getPercentOfVolatility(), configurationTest.getStopLoss(),
                                    configurationTest, tradeLog.getLongBasis(), longconsecutiveTrendBreaks, tradeLog, barList, z);
                            if (longTradeIntent != null) {
                                if (longTradeIntent.getTradeComment().contains("Trend Break") ||
                                        longTradeIntent.getTradeComment().contains("Stop Loss") || longTradeIntent.getTradeComment().contains("Earnings")) {
//                                    for (Trade trade : tradeLog.getActiveTradeList()) {
//                                        if (trade.getAssociatedOptionTrade() != null) {
//                                            double close = getLastforContract(trade.getAssociatedOptionTrade(), bar);
//                                            trade.setOptionClosePrice(close);
//                                        }
//                                    }
                                    exitAllTradesOnSide(tradeLog, bar, barList.get(z - 1), true, longTradeIntent.isEscapeFlag());

                                    //stopLossActive = true;
                                    //longTradeActive = false;
                                } else if (longTradeIntent.getTradeComment().contains("Trim")) {
                                    trimTrade(tradeLog, true, bar, configurationTest, longTradeIntent.getTargetTrade());
                                } else {
//                                    if (longTradeIntent.getTargetTrade().getAssociatedOptionTrade() != null) {
//                                        double close = getLastforContract(longTradeIntent.getTargetTrade().getAssociatedOptionTrade(), bar);
//                                        longTradeIntent.getTargetTrade().setOptionClosePrice(close);
//                                    }

                                    exitOneTrade(tradeLog, true, bar, configurationTest, longTradeIntent.getTargetTrade());


                                }
                               // action.append(longTradeIntent.getTradeComment());
                                longAction = true;
                            }
                        }
                        // SHORT CLOSE //
                        if (tradeLog.isShortActive()) {
                            TradeIntent shortTradeIntent = shortExitConditions(bar, barList.get(z - 1), configurationTest.getPercentOfVolatility(), configurationTest.getStopLoss(),
                                    configurationTest, tradeLog, shortconsecutiveTrendBreaks, barList, z);
                            if (shortTradeIntent != null) {
                                if (shortTradeIntent.getTradeComment().contains("Trend Break") ||
                                        shortTradeIntent.getTradeComment().contains("Stop Loss") || shortTradeIntent.getTradeComment().contains("Earnings")) {


                                    exitAllTradesOnSide(tradeLog, bar, barList.get(z - 1), false, shortTradeIntent.isEscapeFlag());

                                } else {
                                    exitOneTrade(tradeLog, false, bar, configurationTest, shortTradeIntent.getTargetTrade());
                                }
                                //action.append(shortTradeIntent.getTradeComment());
                                shortAction = true;
                            }

                        }
                        boolean openNewLong = false;
                        // OPEN //
                        if (!longAction) {
//                        if ( bar.getTrend() != 0.0 && bar.getTrend() != Double.POSITIVE_INFINITY) {
                            boolean found = false;
                            for (Trade tradeInLog : tradeLog.getActiveTradeList()) {
                                if (!tradeInLog.isLong()) {
                                    found = true;
                                    break;
                                }
                            }
                            if (!found) {
                                TradeIntent longTradeIntent = longOpenConditions(barList.get(z - 2), barList.get(z - 1), bar, null, tradeLog, longconsecutiveTrendConfirm,
                                        configurationTest, stopLossActive, barList, null, z);
                                if (longTradeIntent != null) {
                                    //  OptionContract optionContract = getOptionContract(ticker.getTicker(),bar);
                                    OptionContract optionContract = null;
                                    //System.out.println(bar.getDate() + " has a best option contact of: " + optionContract);
                                    tradeLog.pushNewActiveTrade(createNewTrade(barList, z, bar, true,longTradeIntent.getSizeFactor(), ticker, configurationTest.getDollarPercent(), optionContract, configurationTest,longTradeIntent.getCrossType()));

                                    //  action.append(longTradeIntent.getTradeComment());
                                    openNewLong = true;

                                }
                            }
                            //     else {
                            //     else {
                        }
                        if (!shortAction && !openNewLong) {
                            boolean found = false;
                            for (Trade tradeInLog : tradeLog.getActiveTradeList()) {
                                if (tradeInLog.isLong()) {
                                    found = true;
                                    break;
                                }
                            }
                            //  }
                            // else if (bar.getTrend() != 0.0 && bar.getTrend() != Double.POSITIVE_INFINITY) {
                            if (!found) {
                                TradeIntent shortTradeIntent = shortOpenConditions(barList.get(z - 2), barList.get(z - 1), bar, null, tradeLog,
                                        0, configurationTest,
                                        stopLossActive, barList, null, null, null, z);
                                OptionContract optionContract = null;
                                if (shortTradeIntent != null) {
                                    tradeLog.pushNewActiveTrade(createNewTrade(barList, z, bar, false, shortTradeIntent.getSizeFactor(), ticker, configurationTest.getDollarPercent(), optionContract, configurationTest,0));

                                  //  action.append(shortTradeIntent.getTradeComment());

                                }
                            }
                        }
                }
            }

            double longDollars = 0.0;
            double shortDollars = 0.0;
            double staticChange = 0;
            int tradeSize = tradeLog.getClosedTradeList().size();
            double successfulTrades1 = 0.0;
            double successfulTrades2 = 0.0;
            double successfulTrades3 = 0.0;
            double failedTrades1 = 0.0;
            double failedTrades2 = 0.0;
            double failedTrades3 = 0.0;
            for(int tradeIndex = 0; tradeIndex < tradeSize; tradeIndex++ ){
                Trade closedTrade = tradeLog.getClosedTradeList().get(tradeIndex);
                double mktCap = (double) closedTrade.getCloseBar().getMarketCap();



            //for (Trade closedTrade : tradeLog.getClosedTradeList()) {
                double exit = closedTrade.getClosingPrice();
                double dollarChange;

                // boolean success;
                if (closedTrade.isLong()) {
                    double delta = (((exit - closedTrade.getTradeBasis()) / closedTrade.getTradeBasis()));
                    staticChange = staticChange + ((100 * closedTrade.getPositionSize()  * configurationTest.getDollarPercent()) * delta);
                    // dollarChange = ((dollars * configurationTest.getDollarPercent()) * (((exit - closedTrade.getTradeBasis()) / closedTrade.getTradeBasis())));
                    //dollarChange = ((dollars * closedTrade.getPositionSize()) * (((exit - closedTrade.getTradeBasis()) / closedTrade.getTradeBasis())));
                    dollarChange = ((dollars * closedTrade.getPositionSize()  * configurationTest.getDollarPercent()) * delta);

                    longDollars += dollarChange;
                    dollars = dollars + dollarChange;
                    // endingDollarList.add(dollars);
                    // closedTrade.setEndingDollarTotal(dollars);
                    if (exit > closedTrade.getTradeBasis()) {
                        longsuccess = longsuccess + 1;
                        if(mktCap < 2000000000){
                            successfulTrades1++;
                        }else if(mktCap < 50000000000L){
                            successfulTrades2++;
                        }else if(mktCap > 50000000000L){
                            successfulTrades3++;
                        }
                        //     success = true;
                    } else {
                        longfail = longfail + 1;
                        if(mktCap < 2000000000){
                            failedTrades1++;
                        }else if(mktCap < 50000000000L){
                            failedTrades2++;
                        }else if(mktCap > 50000000000L){
                            failedTrades3++;
                        }
                        //    success = false;
                    }
                } else {
                    double delta = (((closedTrade.getTradeBasis() - exit) / closedTrade.getTradeBasis()));
                    staticChange = staticChange + ((100 * closedTrade.getPositionSize()  * configurationTest.getDollarPercent()) * delta);
                    // dollarChange = ((dollars * configurationTest.getDollarPercent()) * (((closedTrade.getTradeBasis() - exit) / closedTrade.getTradeBasis())));
                    //dollarChange = ((dollars * closedTrade.getPositionSize()) * (((closedTrade.getTradeBasis() - exit) / closedTrade.getTradeBasis())));
                    dollarChange = ((dollars * closedTrade.getPositionSize()  * configurationTest.getDollarPercent()) * delta);

                    shortDollars += dollarChange;
                    dollars = dollars + dollarChange;
                    // endingDollarList.add(dollars);
                    //closedTrade.setEndingDollarTotal(dollars);
                    if (exit < closedTrade.getTradeBasis()) {
                        shortsuccess = shortsuccess + 1;
                        if(mktCap < 2000000000){
                            successfulTrades1++;
                        }else if(mktCap < 50000000000L){
                            successfulTrades2++;
                        }else if(mktCap > 50000000000L){
                            successfulTrades3++;
                        }
                        //    success = true;
                    } else {
                        shortfail = shortfail + 1;
                        if(mktCap < 2000000000){
                            failedTrades1++;
                        }else if(mktCap < 50000000000L){
                            failedTrades2++;
                        }else if(mktCap > 50000000000L){
                            failedTrades3++;
                        }
                        //    success = false;
                    }
                }
            }

            //configurationTest.setVolOfReturn(stockCalculationLibrary.getReturnVariance(endingDollarList));
            Collections.reverse(barList);
            //  Collections.reverse(orderedStrings);
            double volTotal = 0.0;
            int volCount = 0;
//            for (Bar bar : barList) {
//                if (bar.getTrendVol() != 0.0 && !Double.isNaN(bar.getTrendVol())) {
//                    volTotal += bar.getTrendVol();
//                    volCount++;
//                }
//            }
            double successRate = (longsuccess + shortsuccess) / ((longsuccess + shortsuccess) + (longfail + shortfail));
            extracted(configurationTest, dollars, longsuccess, longfail, shortsuccess, shortfail, volTotal, volCount, successRate);
            //configurationTest.setDaysVol(daysVol);

            IndividualStockTest individualStockTest = new IndividualStockTest();
            individualStockTest.setAverageVol(volTotal / volCount);
            individualStockTest.setTicker(ticker.getTicker());
            individualStockTest.setSuccessfulTrades((int) (longsuccess + shortsuccess));
            individualStockTest.setFailedTrades((int) (longfail + shortfail));
            individualStockTest.setSuccessRate(successRate);
            individualStockTest.setDollars(dollars);
            individualStockTest.setTradeLog(tradeLog);
            individualStockTest.setStaticDollars(staticChange);
            individualStockTest.setLongPct((double) longsuccess / (longsuccess + longfail));
            individualStockTest.setShortPct((double) shortsuccess / (shortsuccess + shortfail));
            individualStockTest.setShortDollars(shortDollars);
            individualStockTest.setLongDollars(longDollars);
            individualStockTest.setFailedTrades1(failedTrades1);
            individualStockTest.setFailedTrades2(failedTrades2);
            individualStockTest.setFailedTrades3(failedTrades3);
            individualStockTest.setSuccessfulTrades1(successfulTrades1);
            individualStockTest.setSuccessfulTrades2(successfulTrades2);
            individualStockTest.setSuccessfulTrades3(successfulTrades3);
            if(barList.size()>0) {
                individualStockTest.setLastBar(barList.get(barList.size() - 1));
            }
           // individualStockTest.setLongPossibleProfitable(longsPossibleProfitable);
           // individualStockTest.setShortPossibleProfitable(shortsPossibleProfitable);

            configurationTest.getStockTestList().add(individualStockTest);
//            if(logData){
//                for(int y = 0; y < barList.size(); y++){
//                    Bar bar = barList.get(y);
////                    System.out.println(simpleDateFormat.format(bar.getDate()) + "\t" + bar.getClose() + "\t" +
////                            bar.getMovingTrendLength() + "\t" + bar.getTreasuryRate() + "\t" + dollarBars.get(y).getClose() + "\t" + bar.getVolumeChange()
////                            + "\t" + bar.getDollarCorrelationFactor() + "\t" + bar.getTreasuryCorrelationFactor() + "\t" + bar.getCommodityCorrelationFactor());
//                    System.out.println(simpleDateFormat.format(bar.getDate()) + "\t" + bar.getClose() + "\t" +
//                            bar.getMovingTrendLength() + "\t" + bar.getTreasuryRate() + "\t" + dollarBars.get(y).getClose() + "\t" + bar.getVolumeChange()
//                            + "\t" + bar.getDollarCorrelationFactor() + "\t" + bar.getTreasuryCorrelationFactor() + "\t" + bar.getCommodityCorrelationFactor() + "\t"
//                            +  bar.getSignalSlopeLong() + "\t" + bar.getSignalRocLong() + "\t" + bar.getSignalSlopeShort() + "\t" + bar.getSignalRocShort());
//                }
//            }
        }


        backTestThreadMonitor.threadFinished(threadNum, configurationTestList);
    }

    private void extracted(ConfigurationTest configurationTest, double dollars, double longsuccess, double longfail, double shortsuccess, double shortfail, double volTotal, int volCount, double successRate) {
        complete++;
        now = Instant.now();
        delta = Duration.between(start, now).toMillis();
        rate = ((float) complete / delta) * 1000;
        boolean sizeOne = configurationTestList.size() != 1;

       // if(complete % 250 == 0 || !sizeOne) {
 //           StringBuilder stringBuilder = new StringBuilder();
//            stringBuilder.append(ticker);
//            stringBuilder.append("\n");
//            stringBuilder.append("sum: ").append(dollars).append("\n");
//            stringBuilder.append("L: ").append(longsuccess).append("/").append((longsuccess + longfail)).append("\n");
//            stringBuilder.append("S: ").append(shortsuccess).append("/").append((shortsuccess + shortfail));
//            stringBuilder.append("success rate: ").append(successRate);
//            stringBuilder.append((longsuccess + shortsuccess)).append("/").append((longsuccess + shortsuccess + longfail + shortfail));
//            stringBuilder.append("long: ").append(longsuccess / (longsuccess + longfail));
//
//            System.out.println(stringBuilder);
//            System.out.println("short: " + shortsuccess / (shortsuccess + shortfail));
            //  System.out.println("average Vol: " + volTotal / volCount);
            //System.out.println("accuracy: " + accuracy);
           // System.out.println(ANSI_ORANGE + "Games per second: " + rate + "\n" + ANSI_RESET);
         //   System.out.println("complete: " + complete + "/" + configurationTestList.size());
      //  }
    }

    private void stepFour(StockCalculationLibrary stockCalculationLibrary,  ArrayList<Bar> barList, Bar bar,
                          int index, int correlationDays1, boolean calculatingTrend, int lowBetaIndex, boolean isLong) {
        PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
        // int lookback = bar.getLookback();
        int correlationDays = bar.getMovingTrendLength();
//        if (!calculatingTrend) {
//            correlationDays = bar.getMovingTradeLength();
//        }
        if (bar.getMarketCap() > 50000000000L) {
            correlationDays = correlationDays * 3;
        }
        if (correlationDays > 1) {
            List<Double> priceChanges = new ArrayList<>();
            //    List<Double> historicalVolChanges = new ArrayList<>();
            List<Double> realizedVols = new ArrayList<>();
            List<Double> dollarChanges = new ArrayList<>();
            List<Double> oilChanges = new ArrayList<>();
            List<Double> commodityChanges = new ArrayList<>();
            List<Double> treasury = new ArrayList<>();

            if (index + correlationDays + 14 < barList.size()) {
                for (int k = index; k < index + correlationDays; k++) {
                    Bar current = barList.get(k);
                    // Bar past = barList.get(k + 1);
                    priceChanges.add(current.getClose());

                    realizedVols.add(current.getBaseVolatility());

                    dollarChanges.add(barList.get(k).getDollarRate());
                    oilChanges.add(barList.get(k).getOilValue());
                    treasury.add(barList.get(k).getTreasuryRate());
                    commodityChanges.add(barList.get(k).getCommodityIndexValue());

                }
            }
            //double[] x = convertListToArray(volumes);
            //double[] y1 = convertListToArray(historicalVolChanges);
            //double[] x = volumes.stream().mapToDouble(z -> z).toArray();
            double[] y = convertListToArray(priceChanges);
            //double[] x1 = convertListToArray(discounts);
            //double[] x2 = convertListToArray(impliedVols);
            double[] x3 = convertListToArray(dollarChanges);
            double[] x4 = convertListToArray(realizedVols);
            double[] x5 = convertListToArray(treasury);
            double[] x6 = convertListToArray((commodityChanges));
            //double[] x7 = convertListToArray(fedAssetChanges);
            double[] x8 = convertListToArray(oilChanges);

            if (calculatingTrend) {
                try {
                    if (!isLong) {

                        bar.setDollarCorrelationFactor(pearsonsCorrelation.correlation(x3, y));
                        bar.setRealizedVolCorrelationFactor(pearsonsCorrelation.correlation(x4, y));
                        bar.setTreasuryCorrelationFactor(pearsonsCorrelation.correlation(x5, y));
                        bar.setCommodityCorrelationFactor(pearsonsCorrelation.correlation(x6, y));
                        bar.setOilCorrelationFactor(pearsonsCorrelation.correlation(x8, y));
                    } else {

                        bar.setDollarCorrelationFactor(pearsonsCorrelation.correlation(x3, y));
                        bar.setRealizedVolCorrelationFactor(pearsonsCorrelation.correlation(x4, y));
                        bar.setTreasuryCorrelationFactor(pearsonsCorrelation.correlation(x5, y));
                        bar.setCommodityCorrelationFactor(pearsonsCorrelation.correlation(x6, y));
                        bar.setOilCorrelationFactor(pearsonsCorrelation.correlation(x8, y));
                    }

                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            } else {
                try {

                } catch (Exception ignored) {
                }
            }
        }
    }




    public Trade createNewTrade(List<Bar> barList, int index, Bar bar, boolean isLong, int category, Ticker ticker, double tradeSize, OptionContract optionContract,ConfigurationTest configurationTest, int crossType){
        Bar thirtyBar = null;
        Bar fifteenBar = null;
        if(index + 30 < barList.size()){
            thirtyBar = barList.get(index +30);
        }
        if(index + 15 < barList.size()){
            fifteenBar = barList.get(index +15);
        }

        Double quadReturn = 0.0;

        // Double quadReturn = quadReturns.get(quadNumber);
        int confirmationCount = 0;
//        if(isLong){
//            if(barList.get(index ).getTrend() < barList.get(index ).getClose()){
//                confirmationCount++;
//            }
//            if(barList.get(index - 1).getTrend() < barList.get(index - 1).getClose()){
//                confirmationCount++;
//            }
//            if(barList.get(index - 2).getTrend() < barList.get(index - 2).getClose()){
//                confirmationCount++;
//            }
//        }
//        if(!isLong){
//            try {
//                if (barList.get(index).getTrend() > barList.get(index).getClose()) {
//                    confirmationCount++;
//                }
//                if (barList.get(index - 1).getTrend() > barList.get(index - 1).getClose()) {
//                    confirmationCount++;
//                }
//                if (barList.get(index - 2).getTrend() > barList.get(index - 2).getClose()) {
//                    confirmationCount++;
//                }
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }
        return new Trade(confirmationCount, bar.getClose(), isLong, bar.getDate(), category, bar, ticker, quadReturn, tradeSize,fifteenBar, thirtyBar, optionContract,configurationTest,crossType);
    }

    public void exitAllTradesOnSide(TradeLog tradeLog, Bar bar, Bar priorBar, boolean isLong, boolean escapeFlag){
        double exitPrice = bar.getClose();
        if(escapeFlag){
            exitPrice = 0;
        }
        Date date = bar.getDate();
        if(escapeFlag){
            date = priorBar.getDate();
        }
        tradeLog.closeAllOnSide(isLong, exitPrice, date, bar, escapeFlag);
    }
    public void trimTrade(TradeLog tradeLog, boolean isLong, Bar bar, ConfigurationTest configurationTest, Trade trade){

        // if(isLong) {
        //          if(tradeLog.getActiveTrades() >1) {
        // tradeLog.closeSpecificTrade(trade, bar.getClose(), bar.getDate(), bar);
        //        }
        //  }else{
        tradeLog.trimPosition(trade,bar.getClose(), bar.getDate(), bar);
        //  }

    }
    public void exitOneTrade(TradeLog tradeLog, boolean isLong, Bar bar, ConfigurationTest configurationTest, Trade trade){

        // if(isLong) {
        //          if(tradeLog.getActiveTrades() >1) {
        // tradeLog.closeSpecificTrade(trade, bar.getClose(), bar.getDate(), bar);
        //        }
        //  }else{
        tradeLog.closeSpecificTrade(trade,bar.getClose(), bar.getDate(), bar);
        //  }

    }
    public void exitOneTradeNew(TradeLog tradeLog, boolean isLong, Bar bar){
        if(isLong) {
            tradeLog.closeFirstIn(isLong, (bar.getHigh() + bar.getClose()) / 2, bar.getDate(), bar);
        }else{
            tradeLog.closeFirstIn(isLong, (bar.getLow() + bar.getClose()) / 2, bar.getDate(), bar);

        }
    }
    public void exitAllOptionTradesOnSide(OptionTradeLog optionTradeLog, Bar bar, boolean isLong){
        optionTradeLog.closeAllOnSide(isLong, bar.getVwap(), bar.getDate());
    }
    public boolean exitOneOptionTrade(int index, OptionTradeLog optionTradeLog, boolean isLong, Bar bar, double vwap){
        return optionTradeLog.closeFirstIn(index, isLong, vwap, bar.getDate());
    }
    public double calculateTradeBasis(List<Trade> activeTrades){
        double total = 0;
        for(Trade activeTrade : activeTrades){
            total += activeTrade.getTradeBasis();
        }
        return total/activeTrades.size();
    }
    public void testFractalLookback(List<Bar> barList, int k){
        if(k + 5 < barList.size()) {
            //find first pattern
            List<Bar> subList = barList.subList(k, k + 5);
            //look at each pattern before

            //collect all patterns

            //find the closest pattern occurrence

            //other analysis?
        }
    }
    public static double percentile(List<Double> latencies, double percentile) {
        int index = (int) Math.ceil(percentile / 100.0 * latencies.size());
        if(index > 0) {
            return latencies.get(index - 1);
        }else {
            return 0;
        }
    }
    public TradeIntent longOpenConditions(Bar twoBarsPrior, Bar priorBar, Bar bar, List<QuarterlyQuad> quads, TradeLog tradeLog,
                                          int longconsecutiveTrendConfirm,
                                          ConfigurationTest configurationTest, boolean stopLossActive,
                                          List<Bar> barList, List<Double> allSlopes, int x) {
        //Logic driver for  cross1
        boolean cross1 = false;
        if (tradeLog.getActiveTrades() > 0) {
            int crossType = tradeLog.getActiveTradeList().get(0).getCrossType();
            if (crossType == 1) {
                cross1 = true;
            }
        }

        //Logic driver for cross2
        boolean cross2 = false;
        if (tradeLog.getActiveTrades() > 0) {
            int crossType = tradeLog.getActiveTradeList().get(0).getCrossType();
            if (crossType == 2) {
                cross2 = true;
            }
        }


//        boolean cross2 = false;
//        if (!cross1){
//            if (tradeLog.getActiveTrades() == 0) {
//                if (priorBar.getAlternateSignalSlopeRoc() > 0 && bar.getAlternateSignalSlopeRoc() < 0) {
//                    cross2 = true;
//                }
//            }else{
//                int crossType = tradeLog.getActiveTradeList().get(0).getCrossType();
//                if (crossType == 1) {
//                    cross2 = priorBar.getAlternateSignalSlopeRoc() < 0;
//                }
//            }
//        }


        int category = 0;

//        if ((convertedTimeStamp < (exclusionEnd) || convertedTimeStamp == exclusionEnd) && (convertedTimeStamp > exclusionBegin) || (convertedTimeStamp == exclusionBegin)) {
//            return null;
//        }

        //check if earnings
        boolean tradeCriteria = true;
        //LocalDate convertedDate = bar.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int bound = 3;
        if (x + 3 > barList.size()) {
            bound = barList.size() - x - 1;
        }
        for (int i = 1; i <= bound; i++) {
            if (barList.get(x + i - 1).isEarningsDate()) {
                tradeCriteria = false;
                break;
            }
        }
        double sum = 0;
        int limit;
        if (x > 5) {
            limit = 5;
        } else {
            limit = x;
        }
        long convertedTimeStamp = barList.get(x).getDate().getTime();
        if (bar.getMostRecentEarnings() != 0) {

            /* 98 days  * 24 hours * 3600000 ms per day*/
            long upperBound = bar.getMostRecentEarnings() + (98L * 24 * 3600000);
            long lowerBound = bar.getMostRecentEarnings() + (82L * 24 * 3600000);
            if ((convertedTimeStamp > lowerBound) && (convertedTimeStamp < upperBound)) {
                return null;
            }
        }
        if (bar.isYearAgoEarnings()) {

            /* 98 days  * 24 hours * 3600000 ms per day*/
            //long upperBound = bar.getYearAgoEarnings() + (260L * 24 * 3600000);
           // long lowerBound = bar.getYearAgoEarnings() + (242L * 24 * 3600000);
           // if ((convertedTimeStamp > lowerBound) && (convertedTimeStamp < upperBound)) {
                return null;
           // }
        }
        // if(bar.getMarketCap() != 0){
        if (bar.getMarketCap() < configurationTest.getMovingTrendLength()[7] * 1000000) {
            tradeCriteria = false;
        }


        for (int d = 0; d < limit; d++) {
            sum = sum + (barList.get(x - d).getVolume() * barList.get(x - d).getClose());
        }
        if (sum / limit < configurationTest.getMovingTrendLength()[6] * 1000) {
            tradeCriteria = false;
        }

        double volSlopeThreshold = -0.2;
        double volRocThreshold = 0;
        double signalRocThresholdx = 0;
        double signalValueThreshold = 1.8e-11;
        boolean volatilityRocFlips = true;
        boolean volatilitySlopeFlips = false;
        boolean signalRocFlips = true;
        boolean signalValueFlips = true;
        double priceSlopeThreshold = 0;
        double volumeThreshold = 0;
        double countLimit = 0;
        boolean priceSlopeFlip = false;
        boolean volumeFlip = false;
        if (bar.getBaseLongVolatility() < 75 && bar.getBaseLongVolatility() > 58) {
            volSlopeThreshold = 0;
            volRocThreshold = 0.015;
            signalRocThresholdx = 0;
            signalValueThreshold = -4e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = false;
            priceSlopeThreshold = 1e-11;
            countLimit = 6;
            volumeThreshold = 0.07;
            category = 4;
            if (bar.getMarketCap() < 2000000000) {
                volSlopeThreshold = -0.15;
                // volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                // volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();

                volRocThreshold = 0.005;
                // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                // volatilityRocFlips = configurationTest.isVolatilityRocFlip();

                signalValueThreshold = 1.1e-10;
                // signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                priceSlopeThreshold = -8e-11;

                //priceSlopeThreshold = configurationTest.getIvWeighting();
                // volumeThreshold = configurationTest.getVolumeWeighting();
                category = 1;
            } else if (bar.getMarketCap() < 50000000000L) {
                volSlopeThreshold = 0.07;
                //   volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volRocThreshold = 0.02;
                //  volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                signalValueThreshold = -4e-11;
                //  signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();

                //  signalValueFlips = configurationTest.isSignalValueFlip();
                priceSlopeThreshold = -9e-11;
                //  priceSlopeThreshold = configurationTest.getIvWeighting();

                category = 2;
            } else if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = -0.15;
                volRocThreshold = 0.03;
                category = 3;

            }


        } else if (bar.getBaseLongVolatility() < 58 && bar.getBaseLongVolatility() > 42) {
            volSlopeThreshold = 0.27;
            volRocThreshold = 0.01;
            signalRocThresholdx = 0;
            signalValueThreshold = -6e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = true;
            priceSlopeThreshold = -3e-11;
            countLimit = 6;
            volumeThreshold = -0.01;
            if (bar.getMarketCap() < 2000000000) {
                volSlopeThreshold = 0.3;
                volatilityRocFlips = false;
                volRocThreshold = 0.005;
                volatilitySlopeFlips = true;
                signalValueThreshold = -4e-11;
                priceSlopeThreshold = -5e-11;
                category = 4;
            } else if (bar.getMarketCap() < 50000000000L) {
                volSlopeThreshold = -0.65;
                volatilitySlopeFlips = false;
                volatilityRocFlips = true;
                // volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                // volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                volRocThreshold = 0.01;
                // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                //volatilityRocFlips = configurationTest.isVolatilityRocFlip();
                signalValueThreshold = -7.5e-11;
                priceSlopeThreshold = -7e-11;
                category = 5;
            } else if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = -0.05;
                volRocThreshold = 0;
                category = 6;
                //   volSlopeThreshold = -0.25;
                //   volRocThreshold = ;
//                volRocThreshold = volRocThreshold*configurationTest.getPercentOfVolatility();
//                signalRocThresholdx = 0;
//                signalValueThreshold = signalValueThreshold*configurationTest.getPercentOfVolatility();
//                priceSlopeThreshold = priceSlopeThreshold*configurationTest.getPercentOfVolatility();
//                volumeThreshold = volumeThreshold *configurationTest.getPercentOfVolatility();
            }
//            if(bar.getMarketCap() > 50000000000L){
//                volSlopeThreshold = volSlopeThreshold*0.98;
//                volRocThreshold = volRocThreshold*0.98;
//                signalRocThresholdx = 0;
//                signalValueThreshold = signalValueThreshold*0.98;
//            }
//            volumeThreshold = configurationTest.getVolumeWeighting();
//            volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
//            volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
//            signalRocThresholdx = 0;
//            signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
//            volatilityRocFlips = configurationTest.isVolatilityRocFlip();
//            volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();


        } else if (bar.getBaseLongVolatility() < 42 && bar.getBaseLongVolatility() > 20) {
            volSlopeThreshold = -0.12;
            volRocThreshold = 0.015;
            signalRocThresholdx = 0;
            signalValueThreshold = -6e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = false;
            signalRocFlips = false;
            signalValueFlips = true;
            priceSlopeThreshold = -3e-11;
            countLimit = 6;
            volumeThreshold = -0.03;

            if (bar.getMarketCap() < 2000000000) {
                volSlopeThreshold = -0.05;
                volRocThreshold = 0.005;
                volatilityRocFlips = true;
                volatilitySlopeFlips = false;

                priceSlopeThreshold = 1e-11;
                category = 7;
            } else if (bar.getMarketCap() < 50000000000L) {
                volSlopeThreshold = -0.05;
                volatilitySlopeFlips = true;
                volRocThreshold = 0.015;
                volatilityRocFlips = true;
                priceSlopeThreshold = -8e-11;
                category = 8;
                //  volRocThreshold = 0.025;
            } else if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = -0.25;
                signalValueFlips = true;
                signalValueThreshold = -9e-11;
                priceSlopeThreshold = -4e-11;
                category = 9;
            }
//            volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
//            volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
//            signalRocThresholdx = 0;
//            signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();

        } else if (bar.getBaseLongVolatility() < 20) {
            volSlopeThreshold = 0.27;
            volRocThreshold = -0.015;
            signalRocThresholdx = 0;
            signalValueThreshold = -4e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = true;
            priceSlopeThreshold = -7e-11;
            countLimit = 4;
            volumeThreshold = 0.02;
            if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = volSlopeThreshold * configurationTest.getPercentOfVolatility();
                category = 10;
            }
        }
        if ((cross1 && !cross2) || (!cross1 && !cross2)) {
            if (tradeCriteria) {
                if (tradeLog.getActiveTrades() == 0) {
                    if (bar.getBaseLongVolatility() < 75 && bar.getBaseVolatility() > bar.getBaseLongVolatility()) {
                        boolean cross = false;
                        if (signalRocFlips) {
                            cross = priorBar.getSignalRocLong() > 0 && bar.getSignalRocLong() < 0;
                        } else {
                            cross = priorBar.getSignalRocLong() < 0 && bar.getSignalRocLong() > 0;
                        }
                        if (!cross) {
                            return null;
                        }
                        boolean signalValue = false;
                        if (signalValueFlips) {
                            signalValue = bar.getSignalSlopeLong() > signalValueThreshold;
                        } else {
                            signalValue = bar.getSignalSlopeLong() < signalValueThreshold;
                        }
                        if (!signalValue) {
                            return null;
                        }
                        boolean signalRoc = false;
                        if (signalRocFlips) {
                            signalRoc = bar.getSignalRocLong() < signalRocThresholdx;
                        } else {
                            signalRoc = bar.getSignalRocLong() > signalRocThresholdx;
                        }
                        if (signalRoc) {
                            boolean volatilitySlope = false;
                            if (volatilitySlopeFlips) {
                                volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                            } else {
                                volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                            }
                            if (volatilitySlope) {
                                boolean volatilityRoc = false;
                                if (volatilityRocFlips) {
                                    volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                } else {
                                    volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                }
                                if (volatilityRoc) {
                                    double sizeFactor = 1;
                                    if (bar.getVolumeChange() < volumeThreshold) {
                                        if (bar.getClose() < priorBar.getClose()) {
                                            boolean price = false;
                                            if (priceSlopeFlip) {
                                                price = bar.getPriceSlope() > priceSlopeThreshold;
                                            } else {
                                                price = bar.getPriceSlope() < priceSlopeThreshold;
                                            }
                                            if (price) {
                                                boolean treasuryRoc = false;
                                                if (bar.getTreasuryCorrelationFactor() > 0.95) {
                                                    if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                        treasuryRoc = true;
                                                    }
                                                } else if (bar.getTreasuryCorrelationFactor() < -0.95) {
                                                    if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                        treasuryRoc = true;
                                                    }
                                                } else {
                                                    treasuryRoc = true;
                                                }
                                                if (treasuryRoc) {
                                                    boolean dollarRoc = false;
                                                    if (bar.getDollarCorrelationFactor() > 0.9) {
                                                        if (bar.getDollarSlopeRoc() > 0) {
                                                            dollarRoc = true;
                                                        }
                                                    } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                        if (bar.getDollarSlopeRoc() < 0) {
                                                            dollarRoc = true;
                                                        }
                                                    } else {
                                                        dollarRoc = true;
                                                    }
                                                    if (dollarRoc) {
                                                        boolean dollar = false;
                                                        if (bar.getDollarCorrelationFactor() > 0.9) {
                                                            if (bar.getDollarSlope() < -4e-11) {
                                                                dollar = true;
                                                            }
                                                        } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                            if (bar.getDollarSlope() > -10e-12) {
                                                                dollar = true;
                                                            }
                                                        } else {
                                                            dollar = true;
                                                        }
                                                        if (dollar) {
                                                            boolean commodityRoc = false;
                                                            if (bar.getCommodityCorrelationFactor() > 0.85) {
                                                                if (bar.getCommodityRateOfChange() > -2e-10) {
                                                                    commodityRoc = true;
                                                                }
                                                            } else if (bar.getCommodityCorrelationFactor() < 0.85 * -1) {
                                                                if (bar.getCommodityRateOfChange() < -8e-10) {
                                                                    commodityRoc = true;
                                                                }
                                                            } else {
                                                                commodityRoc = true;
                                                            }
                                                            if (commodityRoc) {
                                                                boolean oilRoc = false;
                                                                if (bar.getOilCorrelationFactor() > 0.95) {
                                                                    if (bar.getOilSlope() > -9e-11) {
                                                                        oilRoc = true;
                                                                    }
                                                                } else if (bar.getOilCorrelationFactor() < 0.95 * -1) {
                                                                    if (bar.getOilSlope() < -3e-11) {
                                                                        oilRoc = true;
                                                                    }
                                                                } else {
                                                                    oilRoc = true;
                                                                }
                                                                if (oilRoc) {
//                                                                boolean oilSlope = false;
//                                                                if (bar.getOilCorrelationFactor() > configurationTest.getRangeThreshold()) {
//                                                                    if (bar.getOilSlopeRoc() < 0) {
//                                                                        oilSlope = true;
//                                                                    }
//                                                                } else if (bar.getOilCorrelationFactor() < configurationTest.getRangeThreshold() * -1) {
//                                                                    if (bar.getOilSlopeRoc() > 0) {
//                                                                        oilSlope = true;
//                                                                    }
//                                                                } else {
//                                                                    oilSlope = true;
//                                                                }
//                                                                if (oilSlope) {
                                                                    //  if(bar.getPriceSlopeRoC() > configurationTest.getFedTotalAssetsWeighting()) {
                                                                    return new TradeIntent("Long Open", "long", "open", category, null, false, 0);
                                                                    //         }
                                                                }
                                                            }
                                                        }
                                                        //   }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                } else {
                    if (bar.getBaseLongVolatility() < 75) {
                        int longCount = 0;
                        for (Trade activeTrade : tradeLog.getActiveTradeList()) {
                            if (activeTrade.isLong()) {
                                longCount++;
                            }
                        }
                        boolean cross = false;
                        if (signalRocFlips) {
                            cross = priorBar.getSignalRocLong() < 0;
                        } else {
                            cross = priorBar.getSignalRocLong() > 0;
                        }
                        if (cross && longCount < countLimit) {
                            boolean signalValue = false;

                            if (signalValueFlips) {
                                signalValue = bar.getSignalSlopeLong() > signalValueThreshold;
                            } else {
                                signalValue = bar.getSignalSlopeLong() < signalValueThreshold;
                            }
                            if (signalValue) {
                                boolean signalRoc = false;
                                // double signalRocThreshold = configurationTest.getLongOpenSignalRocThreshold();
                                if (signalRocFlips) {
                                    signalRoc = bar.getSignalRocLong() < signalRocThresholdx;
                                } else {
                                    signalRoc = bar.getSignalRocLong() > signalRocThresholdx;
                                }
                                if (signalRoc) {
                                    boolean volatilitySlope = false;
                                    if (volatilitySlopeFlips) {
                                        volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold;
                                    } else {
                                        volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold;
                                    }
                                    if (volatilitySlope) {
                                        boolean volatilityRoc = false;

                                        if (volatilityRocFlips) {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold;
                                        } else {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold;
                                        }
                                        if (volatilityRoc) {

                                            double sizeFactor = 1;

                                            if (bar.getClose() < priorBar.getClose()) {
                                                // if (bar.getPriceSlope() < priceSlopeThreshold) {
                                                boolean price = false;
                                                if (priceSlopeFlip) {
                                                    price = bar.getPriceSlope() > priceSlopeThreshold;
                                                } else {
                                                    price = bar.getPriceSlope() < priceSlopeThreshold;
                                                }
                                                if (price) {
                                                    boolean treasuryRoc = false;
                                                    if (bar.getTreasuryCorrelationFactor() > 0.95) {
                                                        if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else if (bar.getTreasuryCorrelationFactor() < -0.95) {
                                                        if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else {
                                                        treasuryRoc = true;
                                                    }
                                                    if (treasuryRoc) {
                                                        boolean dollarRoc = false;
                                                        if (bar.getDollarCorrelationFactor() > 0.9) {
                                                            if (bar.getDollarSlopeRoc() > 0) {
                                                                dollarRoc = true;
                                                            }
                                                        } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                            if (bar.getDollarSlopeRoc() < 0) {
                                                                dollarRoc = true;
                                                            }
                                                        } else {
                                                            dollarRoc = true;
                                                        }
                                                        if (dollarRoc) {
                                                            boolean dollar = false;
                                                            if (bar.getDollarCorrelationFactor() > 0.9) {
                                                                if (bar.getDollarSlope() < 0) {
                                                                    dollar = true;
                                                                }
                                                            } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                                if (bar.getDollarSlope() > 0) {
                                                                    dollar = true;
                                                                }
                                                            } else {
                                                                dollar = true;
                                                            }
                                                            if (dollar) {
//                                                            boolean commodityRoc = false;
//                                                            if (bar.getCommodityCorrelationFactor() > configurationTest.getRangeThreshold()) {
//                                                                if (bar.getCommodityRateOfChange() > 0) {
//                                                                    commodityRoc = true;
//                                                                }
//                                                            } else if (bar.getCommodityCorrelationFactor() < configurationTest.getRangeThreshold() * -1) {
//                                                                if (bar.getCommodityRateOfChange() < 0) {
//                                                                    commodityRoc = true;
//                                                                }
//                                                            } else {
//                                                                commodityRoc = true;
//                                                            }
//                                                            if(commodityRoc) {
                                                                return new TradeIntent("Long Open", "long", "open", category, null, false, 0);
                                                                //  }
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            // }
                                        }
                                    }
                                    // }
                                }
                                // }
                            }
                        }
                    }

                }
            }
        }
        volSlopeThreshold = -0.2;
        volRocThreshold = 0;
         signalRocThresholdx = 0;
         signalValueThreshold = 1.8e-11;
         volatilityRocFlips = true;
         volatilitySlopeFlips = false;
         signalRocFlips = true;
         signalValueFlips = true;
         priceSlopeThreshold = 0;
         volumeThreshold = 0;
         countLimit = 0;
         priceSlopeFlip = false;
         volumeFlip = false;

        if (bar.getBaseLongVolatility() < 75 && bar.getBaseLongVolatility() > 58) {
            volSlopeThreshold = 0;
            volRocThreshold = 0.015;
            signalRocThresholdx = 0;
            signalValueThreshold = -4e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = false;
            priceSlopeThreshold = 1e-11;
            countLimit = 6;
            volumeThreshold = 0.07;
            category = 4;
            if (bar.getMarketCap() < 2000000000) {
                volSlopeThreshold = 0.15;

                volRocThreshold = 0.005;

                signalValueThreshold = -4e-11;
                priceSlopeThreshold = 4e-11;
                category = 15;
            } else if (bar.getMarketCap() < 50000000000L) {
                volSlopeThreshold = 0.07;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                //volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                volRocThreshold = 0.015;
               // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
               // volatilityRocFlips = configurationTest.isVolatilityRocFlip();

                signalValueThreshold = -4e-11;
               // signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();

                //  signalValueFlips = configurationTest.isSignalValueFlip();
                priceSlopeThreshold = -9e-11;
              //  priceSlopeThreshold = configurationTest.getIvWeighting();

                category = 16;
            } else if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = -0.15;

                volRocThreshold = 0.03;

                category = 17;

            }
        } else if (bar.getBaseLongVolatility() < 58 && bar.getBaseLongVolatility() > 42) {
            volSlopeThreshold = 0.27;
            volRocThreshold = 0.01;
            signalRocThresholdx = 0;
            signalValueThreshold = -6e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = true;
            priceSlopeThreshold = -3e-11;
            countLimit = 6;
            volumeThreshold = -0.01;
            if (bar.getMarketCap() < 2000000000) {
                volSlopeThreshold = 0.15;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                //volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                volRocThreshold = -0.01;
                //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volatilityRocFlips = false;

                signalValueThreshold = 4e-11;
                //signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                signalValueFlips = true;
                priceSlopeThreshold = -7e-11;
                //priceSlopeThreshold = configurationTest.getIvWeighting();
                category = 18;
            } else if (bar.getMarketCap() < 50000000000L) {
                volSlopeThreshold = -0.65;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volatilitySlopeFlips = false;

                // volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                // volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                volRocThreshold = 0.02;
                //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volatilityRocFlips = true;
                // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                //volatilityRocFlips = configurationTest.isVolatilityRocFlip();
                signalValueThreshold = -7.5e-11;
                //signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                priceSlopeThreshold = -9e-11;
                //priceSlopeThreshold = configurationTest.getIvWeighting();
                category = 19;
            } else if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = -0.05;
                volRocThreshold = 0;
                category = 20;
                //   volSlopeThreshold = -0.25;
                //   volRocThreshold = ;
//                volRocThreshold = volRocThreshold*configurationTest.getPercentOfVolatility();
//                signalRocThresholdx = 0;
//                signalValueThreshold = signalValueThreshold*configurationTest.getPercentOfVolatility();
//                priceSlopeThreshold = priceSlopeThreshold*configurationTest.getPercentOfVolatility();
//                volumeThreshold = volumeThreshold *configurationTest.getPercentOfVolatility();
            }
        } else if (bar.getBaseLongVolatility() < 42 && bar.getBaseLongVolatility() > 20) {
            volSlopeThreshold = -0.12;
            volRocThreshold = 0.015;
            signalRocThresholdx = 0;
            signalValueThreshold = -6e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = false;
            signalRocFlips = false;
            signalValueFlips = true;
            priceSlopeThreshold = -3e-11;
            countLimit = 6;
            volumeThreshold = -0.03;

            if (bar.getMarketCap() < 2000000000) {
                volSlopeThreshold = -0.1;
               // volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volRocThreshold = 0.01;
               // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
               // volatilityRocFlips = true;
               // volatilitySlopeFlips = false;

                priceSlopeThreshold = 1e-11;
               // signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                signalValueThreshold = -8e-11;
                //priceSlopeThreshold = configurationTest.getIvWeighting();
                category = 26;
            } else if (bar.getMarketCap() < 50000000000L) {
                volSlopeThreshold = -0.05;
                volatilitySlopeFlips = true;
                volRocThreshold = 0.015;
                volatilityRocFlips = true;
                priceSlopeThreshold = -8e-11;
                category = 27;
                //  volRocThreshold = 0.025;
            } else if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = -0.25;
                signalValueFlips = true;
                signalValueThreshold = -9e-11;
                priceSlopeThreshold = -4e-11;
                category = 28;
            }
//            volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
//            volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
//            signalRocThresholdx = 0;
//            signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();

        }else if (bar.getBaseLongVolatility() < 20) {
            volSlopeThreshold = 0.27;
            //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
            volRocThreshold = -0.005;
            //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
            signalRocThresholdx = 0;
            signalValueThreshold = -4e-11;
            //signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = true;
            priceSlopeThreshold = -7e-11;
            countLimit = 4;
            category = 10;
            volumeThreshold = 0.02;
            if (bar.getMarketCap() > 50000000000L) {
                volSlopeThreshold = volSlopeThreshold * configurationTest.getPercentOfVolatility();
                category = 10;
            }
        }




        if ((!cross1 && cross2) || (!cross1 && !cross2)) {
            if (tradeCriteria) {
                if (tradeLog.getActiveTrades() == 0) {
                    if (bar.getBaseLongVolatility() < 75 && bar.getBaseVolatility() > bar.getBaseLongVolatility()) {
                        boolean cross = false;
                        if (signalRocFlips) {
                            cross = priorBar.getAlternateSignalSlopeRoc() > 0 && bar.getAlternateSignalSlopeRoc() < 0;
                        } else {
                            cross = priorBar.getAlternateSignalSlopeRoc() < 0 && bar.getAlternateSignalSlopeRoc() > 0;
                        }
                        if (!cross) {
                            return null;
                        }
                        boolean signalValue = false;
                        if (signalValueFlips) {
                            signalValue = bar.getAlternateSignalSlope() > signalValueThreshold;
                        } else {
                            signalValue = bar.getAlternateSignalSlope() < signalValueThreshold;
                        }
                        if (!signalValue) {
                            return null;
                        }
                        boolean signalRoc = false;
                        if (signalRocFlips) {
                            signalRoc = bar.getAlternateSignalSlopeRoc() < signalRocThresholdx;
                        } else {
                            signalRoc = bar.getAlternateSignalSlopeRoc() > signalRocThresholdx;
                        }
                        if (signalRoc) {
                            boolean volatilitySlope = false;
                            if (volatilitySlopeFlips) {
                                volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                            } else {
                                volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                            }
                            if (volatilitySlope) {
                                boolean volatilityRoc = false;
                                if (volatilityRocFlips) {
                                    volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                } else {
                                    volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                }
                                if (volatilityRoc) {
                                    double sizeFactor = 1;
                                    if (bar.getVolumeChange() < volumeThreshold) {
                                        if (bar.getClose() < priorBar.getClose()) {
                                            boolean price = false;
                                            if (priceSlopeFlip) {
                                                price = bar.getPriceSlope() > priceSlopeThreshold;
                                            } else {
                                                price = bar.getPriceSlope() < priceSlopeThreshold;
                                            }
                                            if (price) {
                                                boolean treasuryRoc = false;
                                                if (bar.getTreasuryCorrelationFactor() > 0.95) {
                                                    if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                        treasuryRoc = true;
                                                    }
                                                } else if (bar.getTreasuryCorrelationFactor() < -0.95) {
                                                    if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                        treasuryRoc = true;
                                                    }
                                                } else {
                                                    treasuryRoc = true;
                                                }
                                                if (treasuryRoc) {
                                                    boolean dollarRoc = false;
                                                    if (bar.getDollarCorrelationFactor() > 0.9) {
                                                        if (bar.getDollarSlopeRoc() > 0) {
                                                            dollarRoc = true;
                                                        }
                                                    } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                        if (bar.getDollarSlopeRoc() < 0) {
                                                            dollarRoc = true;
                                                        }
                                                    } else {
                                                        dollarRoc = true;
                                                    }
                                                    if (dollarRoc) {
                                                        boolean dollar = false;
                                                        if (bar.getDollarCorrelationFactor() > 0.9) {
                                                            if (bar.getDollarSlope() < -4e-11) {
                                                                dollar = true;
                                                            }
                                                        } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                            if (bar.getDollarSlope() > -10e-12) {
                                                                dollar = true;
                                                            }
                                                        } else {
                                                            dollar = true;
                                                        }
                                                        if (dollar) {
                                                            boolean commodityRoc = false;
                                                            if (bar.getCommodityCorrelationFactor() > 0.85) {
                                                                if (bar.getCommodityRateOfChange() > -2e-10) {
                                                                    commodityRoc = true;
                                                                }
                                                            } else if (bar.getCommodityCorrelationFactor() < 0.85 * -1) {
                                                                if (bar.getCommodityRateOfChange() < -8e-10) {
                                                                    commodityRoc = true;
                                                                }
                                                            } else {
                                                                commodityRoc = true;
                                                            }
                                                            if (commodityRoc) {
                                                                boolean oilRoc = false;
                                                                if (bar.getOilCorrelationFactor() > 0.95) {
                                                                    if (bar.getOilSlope() > -9e-11) {
                                                                        oilRoc = true;
                                                                    }
                                                                } else if (bar.getOilCorrelationFactor() < 0.95 * -1) {
                                                                    if (bar.getOilSlope() < -3e-11) {
                                                                        oilRoc = true;
                                                                    }
                                                                } else {
                                                                    oilRoc = true;
                                                                }
                                                                if (oilRoc) {
//                                                                boolean oilSlope = false;
//                                                                if (bar.getOilCorrelationFactor() > configurationTest.getRangeThreshold()) {
//                                                                    if (bar.getOilSlopeRoc() < 0) {
//                                                                        oilSlope = true;
//                                                                    }
//                                                                } else if (bar.getOilCorrelationFactor() < configurationTest.getRangeThreshold() * -1) {
//                                                                    if (bar.getOilSlopeRoc() > 0) {
//                                                                        oilSlope = true;
//                                                                    }
//                                                                } else {
//                                                                    oilSlope = true;
//                                                                }
//                                                                if (oilSlope) {
                                                                    //  if(bar.getPriceSlopeRoC() > configurationTest.getFedTotalAssetsWeighting()) {
                                                                    return new TradeIntent("Long Open", "long", "open", category, null, false, 0);
                                                                    //         }
                                                                }
                                                            }
                                                        }
                                                        //   }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                } else {
                    if (bar.getBaseLongVolatility() < 75) {
                        int longCount = 0;
                        for (Trade activeTrade : tradeLog.getActiveTradeList()) {
                            if (activeTrade.isLong()) {
                                longCount++;
                            }
                        }
                        boolean cross = false;
                        if (signalRocFlips) {
                            cross = priorBar.getAlternateSignalSlopeRoc() < 0;
                        } else {
                            cross = priorBar.getAlternateSignalSlopeRoc() > 0;
                        }
                        if (cross && longCount < countLimit) {
                            boolean signalValue = false;

                            if (signalValueFlips) {
                                signalValue = bar.getAlternateSignalSlope() > signalValueThreshold;
                            } else {
                                signalValue = bar.getAlternateSignalSlope() < signalValueThreshold;
                            }
                            if (signalValue) {
                                boolean signalRoc = false;
                                // double signalRocThreshold = configurationTest.getLongOpenSignalRocThreshold();
                                if (signalRocFlips) {
                                    signalRoc = bar.getAlternateSignalSlopeRoc() < signalRocThresholdx;
                                } else {
                                    signalRoc = bar.getAlternateSignalSlopeRoc() > signalRocThresholdx;
                                }
                                if (signalRoc) {
                                    boolean volatilitySlope = false;
                                    if (volatilitySlopeFlips) {
                                        volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold;
                                    } else {
                                        volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold;
                                    }
                                    if (volatilitySlope) {
                                        boolean volatilityRoc = false;

                                        if (volatilityRocFlips) {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold;
                                        } else {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold;
                                        }
                                        if (volatilityRoc) {

                                            double sizeFactor = 1;

                                            if (bar.getClose() < priorBar.getClose()) {
                                                // if (bar.getPriceSlope() < priceSlopeThreshold) {
                                                boolean price = false;
                                                if (priceSlopeFlip) {
                                                    price = bar.getPriceSlope() > priceSlopeThreshold;
                                                } else {
                                                    price = bar.getPriceSlope() < priceSlopeThreshold;
                                                }
                                                if (price) {
                                                    boolean treasuryRoc = false;
                                                    if (bar.getTreasuryCorrelationFactor() > 0.95) {
                                                        if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else if (bar.getTreasuryCorrelationFactor() < -0.95) {
                                                        if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else {
                                                        treasuryRoc = true;
                                                    }
                                                    if (treasuryRoc) {
                                                        boolean dollarRoc = false;
                                                        if (bar.getDollarCorrelationFactor() > 0.9) {
                                                            if (bar.getDollarSlopeRoc() > 0) {
                                                                dollarRoc = true;
                                                            }
                                                        } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                            if (bar.getDollarSlopeRoc() < 0) {
                                                                dollarRoc = true;
                                                            }
                                                        } else {
                                                            dollarRoc = true;
                                                        }
                                                        if (dollarRoc) {
                                                            boolean dollar = false;
                                                            if (bar.getDollarCorrelationFactor() > 0.9) {
                                                                if (bar.getDollarSlope() < 0) {
                                                                    dollar = true;
                                                                }
                                                            } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                                if (bar.getDollarSlope() > 0) {
                                                                    dollar = true;
                                                                }
                                                            } else {
                                                                dollar = true;
                                                            }
                                                            if (dollar) {
//                                                            boolean commodityRoc = false;
//                                                            if (bar.getCommodityCorrelationFactor() > configurationTest.getRangeThreshold()) {
//                                                                if (bar.getCommodityRateOfChange() > 0) {
//                                                                    commodityRoc = true;
//                                                                }
//                                                            } else if (bar.getCommodityCorrelationFactor() < configurationTest.getRangeThreshold() * -1) {
//                                                                if (bar.getCommodityRateOfChange() < 0) {
//                                                                    commodityRoc = true;
//                                                                }
//                                                            } else {
//                                                                commodityRoc = true;
//                                                            }
//                                                            if(commodityRoc) {
                                                                return new TradeIntent("Long Open", "long", "open", category, null, false, 0);
                                                                //  }
                                                            }
                                                        }
                                                    }
                                                }

                                            }

                                            // }
                                        }
                                    }
                                    // }
                                }
                                // }
                            }
                        }
                    }

                }
            }
        }


        return null;

    }

    public void setPreviousEarningsDates(List<Bar> barList, int x){

        Bar foundBar = null;
        int limit1;
        if(x > 270){
            limit1 = 270;
        }else{
            limit1 = x-1;
        }
        for(int i = 1; i <=limit1-1; i++){
            if(x-i-1 > 0) {
                if (barList.get(x - i - 1).isEarningsDate()) {
                    foundBar = barList.get(x - i - 1);
                    barList.get(x).setMostRecentEarnings(foundBar.getDate().getTime());
                    break;
                }
            }
        }

        //foundBar = null;
        if(x > 256){
            limit1 = 256;
        }else{
            limit1 = x-1;
        }
        //one year ago long = 252 * 1440 *1000
        for(int i = 248; i <=limit1-1; i++){
            if(x-i-1 > 0) {
                if (barList.get(x - i - 1).isEarningsDate()) {
                       // foundBar = barList.get(x - i - 1);
                        barList.get(x).setYearAgoEarnings(true);

                    break;
                }
            }
        }



    }

    public void getVolumeChange(List<Bar> bars, double test, ConfigurationTest configurationTest){
        for(int z = 0; z < bars.size(); z++) {
            if( bars.get(z).getMarketCap() > 50000000000L){
                test = test;
            }
            if(z + test < bars.size()) {

                bars.get(z).setVolume(bars.get(z).getVolume());
                double sum = 0.0;
                for (int x = 0; x < test/2; x++) {
                    sum += bars.get(z + x).getVolume();
                }
                sum = sum / (test/2);
                double oldSum = 0.0;
                for (int x = (int) (test/2); x < test; x++) {
                    oldSum += bars.get(z + x).getVolume();
                }
                oldSum = oldSum / (int) (test/2);

                bars.get(z).setVolumeChange((sum - oldSum) / oldSum);
            }

        }
    }

    public TradeIntent shortOpenConditions(Bar twoBarsPrior, Bar priorBar, Bar bar, List<QuarterlyQuad> quads, TradeLog tradeLog,
                                           int shortconsecutiveTrendConfirm,
                                           ConfigurationTest configurationTest, boolean stopLossActive,
                                           List<Bar> barList, List<Double> allSlopes, List<Double> allPriceSlopes, List<Double> allVolatilitySlopes, int x){

        boolean cross1 = false;
        if (tradeLog.getActiveTrades() > 0) {
            int crossType = tradeLog.getActiveTradeList().get(0).getCrossType();
            if (crossType == 1) {
                cross1 = true;
            }
        }

        //Logic driver for cross2
        boolean cross2 = false;
        if (tradeLog.getActiveTrades() > 0) {
            int crossType = tradeLog.getActiveTradeList().get(0).getCrossType();
            if (crossType == 2) {
                cross2 = true;
            }
        }

        int category = 0;




        long convertedTimeStamp = barList.get(x).getDate().getTime();
        if(bar.getMostRecentEarnings() != 0){

            /* 98 days  * 24 hours * 3600000 ms per day*/
            long upperBound = bar.getMostRecentEarnings() + (98L * 24 * 3600000);
            long lowerBound = bar.getMostRecentEarnings() + (82L * 24 * 3600000);
            if((convertedTimeStamp > lowerBound) && (convertedTimeStamp < upperBound)){
                return null;
            }
        }
        if (bar.isYearAgoEarnings()) {

            /* 98 days  * 24 hours * 3600000 ms per day*/
            //long upperBound = bar.getYearAgoEarnings() + (260L * 24 * 3600000);
            // long lowerBound = bar.getYearAgoEarnings() + (242L * 24 * 3600000);
            // if ((convertedTimeStamp > lowerBound) && (convertedTimeStamp < upperBound)) {
            return null;
            // }
        }
        int limit;
        //check if earnings
        boolean tradeCriteria = true;
        //LocalDate convertedDate = bar.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int bound = 3;
        if(x + 3 > barList.size()){
            bound = barList.size()-x-1;
        }
        for(int i = 1; i <=bound; i++){
            if(barList.get(x+i-1).isEarningsDate()){
                tradeCriteria = false;
                break;
            }
        }
        // if(bar.getMarketCap() != 0){
        if(bar.getMarketCap() < configurationTest.getMovingTrendLength()[7] * 1000000) {
            tradeCriteria = false;
        }
        double sum = 0;
        if(x > 5){
            limit = 5;
        }else{
            limit = x;
        }
        for(int d = 0; d < limit; d++){
            sum = sum + (barList.get(x - d).getVolume() * barList.get(x - d).getClose());
        }
        if(sum/limit < configurationTest.getMovingTrendLength()[6] * 1000){
            tradeCriteria = false;
        }


        //  }
//        for (LocalDate earningsDate : earningsDates) {
//            if (convertedDate.plusDays(1).equals(earningsDate) ||
//                    convertedDate.plusDays(2).equals(earningsDate) ||
//                    convertedDate.plusDays(3).equals(earningsDate) ||
//                    convertedDate.equals(earningsDate)) {
//                tradeCriteria = false;
//                break;
//            }
//        }
        double volSlopeThreshold = -0.2;
        double volRocThreshold = 0;
        double signalRocThresholdx = 0;
        double signalValueThreshold = 1.8e-11;
        boolean volatilityRocFlips = true;
        boolean volatilitySlopeFlips = false;
        boolean signalRocFlips = true;
        boolean signalValueFlips = true;
        boolean priceSlopeFlip = true;
        boolean volumeFlip = true;
        double priceSlopeThreshold = 0;
        double volumeThreashold = 0;
        double countLimit = 0;
        if (bar.getBaseLongVolatility() > 75){

        }else if(bar.getBaseLongVolatility() <70 && bar.getBaseLongVolatility() > 52) {
            volSlopeThreshold = 0.25;
            volRocThreshold = 0.016;
            signalRocThresholdx = 0;
            signalValueThreshold = 8e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = true;
            signalValueFlips = false;
            priceSlopeThreshold = 6e-11;
            volumeThreashold = -0.04;
            priceSlopeFlip = true;
            volumeFlip = false;
            countLimit = 3;
            if(bar.getMarketCap() < 2000000000){
                 category = 11;
                volSlopeThreshold = -0.3;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volatilitySlopeFlips = true;
                volRocThreshold = 0.025;
               // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volatilityRocFlips = true;
                signalValueThreshold = 8e-11;
                //signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                priceSlopeThreshold = 5e-11;
                //priceSlopeThreshold = configurationTest.getIvWeighting();
            }else if(bar.getMarketCap() < 50000000000L){
                volSlopeThreshold = -0.55;
                volRocThreshold = -0.01;
                volatilityRocFlips = false;
                volatilitySlopeFlips = false;
                signalValueThreshold = -8e-11;
                signalValueFlips = true;
                category = 12;
            }else if(bar.getMarketCap() > 50000000000L){
                category = 13;

            }


        }else if(bar.getBaseLongVolatility() <50 && bar.getBaseLongVolatility() > 40) {
            volSlopeThreshold = 0.04;
            volRocThreshold = 0.01;
            signalRocThresholdx = 0;
            signalValueThreshold = 4e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = true;
            signalValueFlips = false;
            priceSlopeThreshold = 1e-11;
            volumeThreashold = -0.05;
            priceSlopeFlip = true;
            volumeFlip = false;
            countLimit = 3;
            if(bar.getMarketCap() < 2000000000){
                volSlopeThreshold = -0.15;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
               // volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                volRocThreshold = 0.01;
                //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
               // volatilityRocFlips = configurationTest.isVolatilityRocFlip();
                signalValueThreshold = 1.1e-10;

                priceSlopeThreshold = -10e-12;

                category = 14;
                //volRocThreshold = 0.015;
            }else if(bar.getMarketCap() < 50000000000L){
                volSlopeThreshold = 0.75;
                volRocThreshold = 0.006;
                signalValueThreshold = 1.2e-10;
                priceSlopeThreshold = 6e-11;
                category = 21;
                //  volRocThreshold = 0.025;
            }else if(bar.getMarketCap() > 50000000000L) {
                priceSlopeThreshold = -7e-11;
                category = 22;
            }

        }else if(bar.getBaseLongVolatility() < 40 && bar.getBaseLongVolatility() > 30) {
            volSlopeThreshold = -0.13;
            volRocThreshold = 0.005;
            signalRocThresholdx = 0;
            signalValueThreshold = 2E-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = true;
            signalValueFlips = false;
            priceSlopeThreshold = 1e-10;
            volumeThreashold = -0.01;
            priceSlopeFlip = false;
            volumeFlip = true;
            countLimit = 3;
            if(bar.getMarketCap() < 2000000000){
                volRocThreshold = 0.015;
                //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volSlopeThreshold = 0.1;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volatilityRocFlips = true;
                //volatilityRocFlips = configurationTest.isVolatilitySlopeFlips();
                volatilitySlopeFlips = false;
                //volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                signalValueThreshold = -6e-11;
                //signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                signalValueFlips = true;
                priceSlopeThreshold = 8e-11;
                //priceSlopeThreshold = configurationTest.getIvWeighting();
                category = 23;
            }else if(bar.getMarketCap() < 50000000000L){
                volRocThreshold = 0.01;
                //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volSlopeThreshold = -0.1;
                //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volatilityRocFlips = false;
                //volatilityRocFlips = configurationTest.isVolatilitySlopeFlips();
                volatilitySlopeFlips = true;
                //volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                signalValueThreshold = 6e-11;
                signalValueFlips = false;
                priceSlopeThreshold = 8e-11;
                category = 24;
            }else if(bar.getMarketCap() > 50000000000L){
                volRocThreshold = -0.005;
               // volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volSlopeThreshold = -0.05;
               // volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volatilityRocFlips = false;
               // volatilityRocFlips = configurationTest.isVolatilitySlopeFlips();
                volatilitySlopeFlips = true;
               // volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
                signalValueThreshold = -8e-11;
                signalValueFlips = false;
                category = 25;


            }
        }else if(bar.getBaseLongVolatility() < 30 && bar.getBaseLongVolatility() > 20){
            volSlopeThreshold = 0.00;
            volRocThreshold = 0.00;
            signalRocThresholdx = 0;
            signalValueThreshold = 5E-11;
            volatilityRocFlips = false;
            volatilitySlopeFlips = false;
            signalRocFlips = true;
            signalValueFlips = false;
            priceSlopeThreshold = -8e-11;
            volumeThreashold = -0.02;
            priceSlopeFlip = true;
            volumeFlip = true;
            countLimit = 3;
            if(bar.getMarketCap() < 50000000000L){
                priceSlopeThreshold = 4e-11;
            }
        }else if(bar.getBaseLongVolatility() < 20 && bar.getBaseLongVolatility() > 18){
            //    volSlopeThreshold = -0.1;
            //     volRocThreshold = 0.01;
            signalRocThresholdx = 0;
            signalValueThreshold = -5e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = false;
            signalValueFlips = false;
            priceSlopeThreshold = 5e-10;
            volumeThreashold = 0.01;
            priceSlopeFlip = false;
            volumeFlip = true;
            countLimit = 2;
        }
        if ((cross1 && !cross2) || (!cross1 && !cross2)) {
            if (tradeCriteria) {
                if (tradeLog.getActiveTrades() == 0) {
                    if (bar.getBaseLongVolatility() < 72 && bar.getBaseLongVolatility() > 22 && bar.getBaseVolatility() > bar.getBaseLongVolatility()) {
                        boolean cross = false;
                        if (signalRocFlips) {
                            cross = priorBar.getSignalRocLong() > 0 && bar.getSignalRocLong() < 0;
                        } else {
                            cross = priorBar.getSignalRocLong() < 0 && bar.getSignalRocLong() > 0;
                        }
                        if (cross) {
                            boolean signalValue = false;

                            if (signalValueFlips) {
                                signalValue = bar.getSignalSlopeLong() > signalValueThreshold;
                            } else {
                                signalValue = bar.getSignalSlopeLong() < signalValueThreshold;
                            }
                            if (signalValue) {
                                boolean signalRoc = false;
                                if (signalRocFlips) {
                                    signalRoc = bar.getSignalRocLong() < signalRocThresholdx;
                                } else {
                                    signalRoc = bar.getSignalRocLong() > signalRocThresholdx;
                                }
                                if (signalRoc) {
                                    boolean volatilitySlope = false;
                                    if (volatilitySlopeFlips) {
                                        volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                                    } else {
                                        volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                                    }
                                    if (volatilitySlope) {
                                        boolean volatilityRoc = false;

                                        if (volatilityRocFlips) {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                        } else {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                        }
                                        if (volatilityRoc) {

                                            double sizeFactor = 1;
                                            boolean volume = false;
                                            if (volumeFlip) {
                                                volume = bar.getVolumeChange() < volumeThreashold;
                                            } else {
                                                volume = bar.getVolumeChange() > volumeThreashold;
                                            }
                                            if (volume) {
                                                if (bar.getClose() > priorBar.getClose()) {
                                                    boolean price = false;
                                                    if (priceSlopeFlip) {
                                                        price = bar.getPriceSlope() > priceSlopeThreshold;
                                                    } else {
                                                        price = bar.getPriceSlope() < priceSlopeThreshold;
                                                    }
                                                    if (price) {
                                                        boolean treasuryRoc = false;
                                                        if (bar.getTreasuryCorrelationFactor() < 0.9) {
                                                            if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                                treasuryRoc = true;
                                                            }
                                                        } else if (bar.getTreasuryCorrelationFactor() > -0.9) {
                                                            if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                                treasuryRoc = true;
                                                            }
                                                        } else {
                                                            treasuryRoc = true;
                                                        }
                                                        if (treasuryRoc) {
                                                            boolean dollarRoc = false;
                                                            if (bar.getDollarCorrelationFactor() > 0.9) {
                                                                if (bar.getDollarSlopeRoc() > 0) {
                                                                    dollarRoc = true;
                                                                }
                                                            } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                                if (bar.getDollarSlopeRoc() < 0) {
                                                                    dollarRoc = true;
                                                                }
                                                            } else {
                                                                dollarRoc = true;
                                                            }
                                                            if (dollarRoc) {
                                                                return new TradeIntent("Short Open", "short", "open", category, null, false, 1);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    int shortCount = 0;
                    if (bar.getBaseLongVolatility() < 72 && bar.getBaseLongVolatility() > 22 && bar.getBaseVolatility() > bar.getBaseLongVolatility()) {
                        for (Trade activeTrade : tradeLog.getActiveTradeList()) {
                            if (!activeTrade.isLong()) {
                                shortCount++;
                            }
                        }
                        boolean cross = false;
                            if (signalRocFlips) {
                                cross = priorBar.getSignalRocLong() < 0;
                            } else {
                                cross = priorBar.getSignalRocLong() > 0;
                            }
                        if (cross && shortCount < countLimit) {
                            boolean signalValue = false;

                            if (signalValueFlips) {
                                signalValue = bar.getSignalSlopeLong() > signalValueThreshold;
                            } else {
                                signalValue = bar.getSignalSlopeLong() < signalValueThreshold;
                            }
                            if (signalValue) {
                                boolean signalRoc = false;
                                // double signalRocThreshold = configurationTest.getLongOpenSignalRocThreshold();
                                if (signalRocFlips) {
                                    signalRoc = bar.getSignalRocLong() < signalRocThresholdx;
                                } else {
                                    signalRoc = bar.getSignalRocLong() > signalRocThresholdx;
                                }
                                if (signalRoc) {
                                    boolean volatilitySlope = false;
                                    if (volatilitySlopeFlips) {
                                        volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold;
                                    } else {
                                        volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold;
                                    }
                                    if (volatilitySlope) {
                                        boolean volatilityRoc = false;

                                        if (volatilityRocFlips) {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold;
                                        } else {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold;
                                        }
                                        if (volatilityRoc) {

                                            double sizeFactor = 1;

                                            if (bar.getClose() > priorBar.getClose()) {
                                                boolean price = false;
                                                if (priceSlopeFlip) {
                                                    price = bar.getPriceSlope() > priceSlopeThreshold;
                                                } else {
                                                    price = bar.getPriceSlope() < priceSlopeThreshold;
                                                }
                                                if (price) {
                                                    boolean treasuryRoc = false;
                                                    if (bar.getTreasuryCorrelationFactor() < 0.9) {
                                                        if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else if (bar.getTreasuryCorrelationFactor() > -0.9) {
                                                        if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else {
                                                        treasuryRoc = true;
                                                    }
                                                    if (treasuryRoc) {

                                                        return new TradeIntent("Short Open", "short", "open", category, null, false, 1);


                                                    }
                                                }

                                            }

                                        }
                                    }
                                }
                            }
                        }

                    }

                }
            }
        }
         volSlopeThreshold = -0.2;
         volRocThreshold = 0;
         signalRocThresholdx = 0;
         signalValueThreshold = 1.8e-11;
         volatilityRocFlips = true;
         volatilitySlopeFlips = false;
         signalRocFlips = true;
         signalValueFlips = true;
         priceSlopeFlip = true;
         volumeFlip = true;
         priceSlopeThreshold = 0;
         volumeThreashold = 0;
         countLimit = 0;
        if (bar.getBaseLongVolatility() > 75){

        }else if(bar.getBaseLongVolatility() <70 && bar.getBaseLongVolatility() > 52) {
            volSlopeThreshold = 0.25;
            volRocThreshold = 0.016;
            signalRocThresholdx = 0;
            signalValueThreshold = 8e-11;
            volatilityRocFlips = true;
            volatilitySlopeFlips = true;
            signalRocFlips = true;
            signalValueFlips = false;
            priceSlopeThreshold = 6e-11;
            volumeThreashold = -0.04;
            priceSlopeFlip = true;
            volumeFlip = false;
            countLimit = 3;
            if(bar.getMarketCap() < 2000000000){
                category = 30;
                volSlopeThreshold = 0.05;
                volatilitySlopeFlips = false;
                volRocThreshold = -0.005;
                volatilityRocFlips = false;
                signalValueThreshold = -5e-11;
                signalValueFlips = true;
                priceSlopeThreshold = 4e-11;


            }else if(bar.getMarketCap() < 50000000000L){
                volSlopeThreshold = -0.5;
               //volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
                volatilitySlopeFlips = true;
                volRocThreshold = -0.01;
                //volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
                volatilityRocFlips = false;
                signalValueThreshold = -8e-11;
               // signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
                signalValueFlips = false;
                priceSlopeThreshold = 7e-11;
                category = 31;
            }else if(bar.getMarketCap() > 50000000000L){
                category = 13;

            }


        }
//        else if(bar.getBaseLongVolatility() <50 && bar.getBaseLongVolatility() > 40) {
//            volSlopeThreshold = 0.04;
//            volRocThreshold = 0.01;
//            signalRocThresholdx = 0;
//            signalValueThreshold = 4e-11;
//            volatilityRocFlips = true;
//            volatilitySlopeFlips = true;
//            signalRocFlips = true;
//            signalValueFlips = false;
//            priceSlopeThreshold = 1e-11;
//            volumeThreashold = -0.05;
//            priceSlopeFlip = true;
//            volumeFlip = false;
//            countLimit = 3;
//            if (bar.getMarketCap() < 2000000000) {
//                volSlopeThreshold = -0.15;
//                volSlopeThreshold = configurationTest.getLongOpenVolatilitySlopeThreshold();
//                volatilitySlopeFlips = configurationTest.isVolatilitySlopeFlips();
//                volRocThreshold = 0.01;
//                volRocThreshold = configurationTest.getLongOpenVolatilityRocThreshold();
//                volatilityRocFlips = configurationTest.isVolatilityRocFlip();
//                signalValueThreshold = 1.1e-10;
//                signalValueThreshold = configurationTest.getLongOpenSignalValueThreshold();
//                priceSlopeThreshold = -10e-12;
//                priceSlopeThreshold = configurationTest.getIvWeighting();
//                category = 32;
//                //volRocThreshold = 0.015;
//            } else if (bar.getMarketCap() < 50000000000L) {
//                volSlopeThreshold = 0.75;
//                volRocThreshold = 0.006;
//                signalValueThreshold = 1.2e-10;
//                priceSlopeThreshold = 6e-11;
//                category = 33;
//                //  volRocThreshold = 0.025;
//            } else if (bar.getMarketCap() > 50000000000L) {
//                priceSlopeThreshold = -7e-11;
//                category = 34;
//            }
//        }
        if ((!cross1 && cross2) || (!cross1 && !cross2)) {
            if (tradeCriteria && bar.getBaseLongVolatility() <70 && bar.getBaseLongVolatility() > 52 && bar.getMarketCap() < 50000000000L) {
                if (tradeLog.getActiveTrades() == 0) {
                    if (bar.getBaseLongVolatility() < 72 && bar.getBaseLongVolatility() > 22 && bar.getBaseVolatility() > bar.getBaseLongVolatility()) {
                        boolean cross = false;
                        //int crossType = 0;
                        if (signalRocFlips) {
                            cross = priorBar.getAlternateSignalSlopeRoc() > 0 && bar.getAlternateSignalSlopeRoc() < 0;
                            // crossType = 1;
                        } else {
                            cross = priorBar.getAlternateSignalSlopeRoc() < 0 && bar.getAlternateSignalSlopeRoc() > 0;
                            // crossType = 1;
                        }
                        if (cross) {
                            boolean signalValue = false;

                            if (signalValueFlips) {
                                signalValue = bar.getAlternateSignalSlope() > signalValueThreshold;
                            } else {
                                signalValue = bar.getAlternateSignalSlope() < signalValueThreshold;
                            }
                            if (signalValue) {
                                boolean signalRoc = false;
                                // double signalRocThreshold = configurationTest.getLongOpenSignalRocThreshold();
                                if (signalRocFlips) {
                                    signalRoc = bar.getAlternateSignalSlopeRoc() < signalRocThresholdx;
                                } else {
                                    signalRoc = bar.getAlternateSignalSlopeRoc() > signalRocThresholdx;
                                }
                                if (signalRoc) {
                                    //  if(bar.getSignalSlope()  > percentile(allSlopes, configurationTest.getMovingTrendLength()[0])) {
                                    boolean volatilitySlope = false;
                                    if (volatilitySlopeFlips) {
                                        volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                                    } else {
                                        volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold && bar.getVolatilitySlopeLong() != 0;
                                    }
                                    if (volatilitySlope) {
                                        boolean volatilityRoc = false;

                                        if (volatilityRocFlips) {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                        } else {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold && bar.getVolatilitySlopeRoCLong() != 0;
                                        }
                                        if (volatilityRoc) {

                                            double sizeFactor = 1;
                                            boolean volume = false;
                                            if (volumeFlip) {
                                                volume = bar.getVolumeChange() < volumeThreashold;
                                            } else {
                                                volume = bar.getVolumeChange() > volumeThreashold;
                                            }
                                            if (volume) {
                                                if (bar.getClose() > priorBar.getClose()) {
                                                    boolean price = false;
                                                    if (priceSlopeFlip) {
                                                        price = bar.getPriceSlope() > priceSlopeThreshold;
                                                    } else {
                                                        price = bar.getPriceSlope() < priceSlopeThreshold;
                                                    }
                                                    if (price) {
                                                        boolean treasuryRoc = false;
                                                        if (bar.getTreasuryCorrelationFactor() < 0.9) {
                                                            if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                                treasuryRoc = true;
                                                            }
                                                        } else if (bar.getTreasuryCorrelationFactor() > -0.9) {
                                                            if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                                treasuryRoc = true;
                                                            }
                                                        } else {
                                                            treasuryRoc = true;
                                                        }
                                                        if (treasuryRoc) {
                                                            boolean dollarRoc = false;
                                                            if (bar.getDollarCorrelationFactor() > 0.9) {
                                                                if (bar.getDollarSlopeRoc() > 0) {
                                                                    dollarRoc = true;
                                                                }
                                                            } else if (bar.getDollarCorrelationFactor() < -0.9) {
                                                                if (bar.getDollarSlopeRoc() < 0) {
                                                                    dollarRoc = true;
                                                                }
                                                            } else {
                                                                dollarRoc = true;
                                                            }
                                                            if (dollarRoc) {
                                                                return new TradeIntent("Short Open", "short", "open", category, null, false, 2);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    int shortCount = 0;
                    if (bar.getBaseLongVolatility() < 72 && bar.getBaseLongVolatility() > 22 && bar.getBaseVolatility() > bar.getBaseLongVolatility()) {
                        for (Trade activeTrade : tradeLog.getActiveTradeList()) {
                            if (!activeTrade.isLong()) {
                                shortCount++;
                            }
                        }
                        boolean cross = false;
                            if (signalRocFlips) {
                                cross = priorBar.getAlternateSignalSlopeRoc() < 0;
                            } else {
                                cross = priorBar.getAlternateSignalSlopeRoc() > 0;
                            }
                        if (cross && shortCount < countLimit) {
                            boolean signalValue = false;

                            if (signalValueFlips) {
                                signalValue = bar.getAlternateSignalSlope() > signalValueThreshold;
                            } else {
                                signalValue = bar.getAlternateSignalSlope() < signalValueThreshold;
                            }
                            if (signalValue) {
                                boolean signalRoc = false;
                                // double signalRocThreshold = configurationTest.getLongOpenSignalRocThreshold();
                                if (signalRocFlips) {
                                    signalRoc = bar.getAlternateSignalSlopeRoc() < signalRocThresholdx;
                                } else {
                                    signalRoc = bar.getAlternateSignalSlopeRoc() > signalRocThresholdx;
                                }
                                if (signalRoc) {
                                    boolean volatilitySlope = false;
                                    if (volatilitySlopeFlips) {
                                        volatilitySlope = bar.getVolatilitySlopeLong() < volSlopeThreshold;
                                    } else {
                                        volatilitySlope = bar.getVolatilitySlopeLong() > volSlopeThreshold;
                                    }
                                    if (volatilitySlope) {
                                        boolean volatilityRoc = false;

                                        if (volatilityRocFlips) {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() < volRocThreshold;
                                        } else {
                                            volatilityRoc = bar.getVolatilitySlopeRoCLong() > volRocThreshold;
                                        }
                                        if (volatilityRoc) {

                                            double sizeFactor = 1;

                                            if (bar.getClose() > priorBar.getClose()) {
                                                boolean price = false;
                                                if (priceSlopeFlip) {
                                                    price = bar.getPriceSlope() > priceSlopeThreshold;
                                                } else {
                                                    price = bar.getPriceSlope() < priceSlopeThreshold;
                                                }
                                                if (price) {
                                                    boolean treasuryRoc = false;
                                                    if (bar.getTreasuryCorrelationFactor() < 0.9) {
                                                        if (bar.getTreasuryYieldSlopeRoc() < 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else if (bar.getTreasuryCorrelationFactor() > -0.9) {
                                                        if (bar.getTreasuryYieldSlopeRoc() > 0) {
                                                            treasuryRoc = true;
                                                        }
                                                    } else {
                                                        treasuryRoc = true;
                                                    }
                                                    if (treasuryRoc) {

                                                        return new TradeIntent("Short Open", "short", "open", category, null, false, 2);
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }
                            }
                        }

                    }

                }
            }
        }
        return null;

    }


    public TradeIntent longExitConditions(Bar bar, Bar priorBar, double percentageOfVolatility, double stopLossPercentage,
                                          ConfigurationTest configurationTest, double longTradeBasis, int longConsecutiveTrendBreaks, TradeLog tradeLog, List<Bar> bars, int z){

        boolean earningsFound = false;
        long timestamp = bar.getDate().getTime();
        long priortimestamp = priorBar.getDate().getTime();
        if(priortimestamp + (3600000*24*15) < (timestamp)){
            return new TradeIntent("Trend Break", "long", "close", 1, null,true,0);
        }

        int bound = 3;
        if(z + 3 > bars.size()){
            bound = bars.size()-z-1;
        }
        for(int i = 1; i <=bound; i++){
            if(bars.get(z+i-1).isEarningsDate()){
                earningsFound = true;
                break;
            }
        }
        boolean crossBoolean;
        crossBoolean = (priorBar.getSignalRocLong() > 0 && bar.getSignalRocLong() < 0)|| (priorBar.getSignalRocLong() < 0 && bar.getSignalRocLong() > 0);
        if (crossBoolean ||  earningsFound) {
            return new TradeIntent("Trend Break", "long", "close", 1, null, false,0);
        } else {
            for (Trade trade : tradeLog.getActiveTradeList()) {
                //check for trend break first


                double target = ((double) configurationTest.getLongExitTarget());
                double stopTarget = configurationTest.getStopLoss();
                if (bar.getBaseVolatility() < configurationTest.getLongExitVolatility1()) {
                    target = target * 0.20;
                    stopTarget = stopTarget*1.25;
                } else if (bar.getBaseVolatility() < configurationTest.getLongExitVolatility2()) {
                    target = target * 0.50;
                    stopTarget = stopTarget*1.5;
                } else if (bar.getBaseVolatility() < configurationTest.getLongExitVolatility3()) {
                    target = target * 0.75;
                    stopTarget = stopTarget *1.75;
                } else {
                    target = target * 1;
                    stopTarget = stopTarget * 2;
                }
                double profitTarget = (trade.getTradeBasis() * (1 + target));

                if (bar.getClose() > profitTarget) {

                    return new TradeIntent("Take Profit", "long", "close", 1, trade,false,0);
                }
                if(bar.getClose() < trade.getTradeBasis()*(1-stopTarget)){
                    return new TradeIntent("Take Profit", "long", "close", 1, trade,false,0);
                }
                if(bar.getBaseLongVolatility() - trade.getOpeningLongVolatilitySignalLong() > 10){
                    return new TradeIntent("Take Profit", "long", "close", 1, trade,false,0);

                }
            }
        }
        return null;

    }
    public TradeIntent shortExitConditions(Bar bar, Bar priorBar, double percentageOfVolatility, double stopLossPercentage,
                                           ConfigurationTest configurationTest, TradeLog tradeLog, int shortConsecutiveTrendBreaks, List<Bar> bars, int z){

        boolean earningsFound = false;
        //LocalDate convertedDate = bar.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long timestamp = bar.getDate().getTime();
        //LocalDate priorConvertedDate = priorBar.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        long priortimestamp = priorBar.getDate().getTime();
        if(priortimestamp + (3600000*24*15) < (timestamp)){
            return new TradeIntent("Trend Break", "long", "close", 1, null,true,0);
        }
        int bound = 3;
        if(z + 3 > bars.size()){
            bound = bars.size()-z-1;
        }
        for(int i = 1; i <=bound; i++){
            if(bars.get(z+i-1).isEarningsDate()){
                earningsFound = true;
                break;
            }
        }

        boolean crossBoolean;
        // if (!configurationTest.isSignalRocFlips()) {
        crossBoolean = (priorBar.getSignalRocLong() > 0 && bar.getSignalRocLong() < 0) ||
                // } else {
                (priorBar.getSignalRocLong() < 0 && bar.getSignalRocLong() > 0);
        //  }
        if(crossBoolean || earningsFound){
            return new TradeIntent("Trend Break", "short", "close",1, null,false,0);
        }else{
            for(Trade trade : tradeLog.getActiveTradeList()){
                double target =((double)configurationTest.getShortExitTarget());
                double stopTarget = 0.08;
                if(bar.getBaseVolatility() > configurationTest.getShortExitVolatility1()){
                    target = target * 0.25;
                    stopTarget = stopTarget*1.25;
                }else if(bar.getBaseVolatility() > configurationTest.getShortExitVolatility2()){
                    target = target * 0.5;
                    stopTarget = stopTarget*1.5;
                }else if(bar.getBaseVolatility() > configurationTest.getShortExitVolatility3()){
                    target = target * 0.75;
                    stopTarget = stopTarget*1.75;
                }else{
                    target = target * 1;
                    stopTarget = stopTarget*2;
                }
                double profitTarget = (tradeLog.getShortBasis() * (1-target));
                if (bar.getClose() < profitTarget) {
                    return new TradeIntent("Take Profit", "short", "close", 1, trade,false,0);
                }
                double stop = (tradeLog.getShortBasis() * (1+stopTarget));
                if (bar.getClose() > stop) {
                    return new TradeIntent("Take Profit", "short", "close", 1, trade,false,0);
                }
            }
        }

        return null;
    }

    public void calculateDaysVolTriggerSlope(List<Bar> barList, int daysSlope, int secondSlopeDays, int volatilitySlopeDays, boolean isLong, ConfigurationTest configurationTest){

        for(int i = 0; i < barList.size(); i++) {
            int modDaySlope = daysSlope;
            int modSecondSlope = secondSlopeDays;
            if(barList.get(i).getMarketCap() < 2000000000){
                modDaySlope = modDaySlope;
            }else if(barList.get(i).getMarketCap() < 50000000000L){
                modDaySlope = modDaySlope;
            }else if(barList.get(i).getMarketCap() > 50000000000L){
                modDaySlope = (int) (modDaySlope*(configurationTest.getMovingTrendLength()[5]*0.01));
                modSecondSlope = (int) (modSecondSlope*(configurationTest.getMovingTrendLength()[5]*0.01));
            }
            if(i < barList.size() - modDaySlope){
                List<Long> dateLongs = new ArrayList<>();
                List<Double> oneMonthDoubles = new ArrayList<>();
                List<Double> treasuryDoubles = new ArrayList<>();
                List<Double> oilDoubles = new ArrayList<>();
                List<Double> commodityDoubles = new ArrayList<>();
                List<Double> closeDoubles = new ArrayList<>();
                for(int x = 1; x <= modDaySlope; x++){
                    oneMonthDoubles.add(barList.get(i + x).getDollarCorrelationFactor());
                    treasuryDoubles.add(barList.get(i + x).getTreasuryCorrelationFactor());
                    oilDoubles.add(barList.get(i + x).getOilCorrelationFactor());
                    commodityDoubles.add(barList.get(i + x).getCommodityIndexValue());
                    dateLongs.add(barList.get(i + x).getDate().getTime());
                    closeDoubles.add(barList.get(i + x).getClose());

                }
                SimpleRegression dollarRegression = new SimpleRegression();
                SimpleRegression treasuryRegression = new SimpleRegression();
                SimpleRegression oilRegression = new SimpleRegression();
                SimpleRegression comoddityRegression = new SimpleRegression();
                SimpleRegression closeRegression = new SimpleRegression();

                for(int z = 0; z <dateLongs.size(); z++){
                    dollarRegression.addData(dateLongs.get(z),oneMonthDoubles.get(z));
                    treasuryRegression.addData(dateLongs.get(z),treasuryDoubles.get(z));
                    oilRegression.addData(dateLongs.get(z),oilDoubles.get(z));
                    comoddityRegression.addData(dateLongs.get(z),commodityDoubles.get(z));
                    closeRegression.addData(dateLongs.get(z),closeDoubles.get(z));

                }

                barList.get(i).setPriceSlope(closeRegression.getSlope());
                barList.get(i).setTreasuryYieldSlope(treasuryRegression.getSlope());
                barList.get(i).setOilSlope(oilRegression.getSlope());
                barList.get(i).setCommoditySlope(comoddityRegression.getSlope());
                barList.get(i).setDollarSlope(dollarRegression.getSlope());
                if(isLong) {
                    barList.get(i).setSignalSlopeLong(dollarRegression.getSlope() + treasuryRegression.getSlope());
                }

            }
        }
        try{
        for(int i = 0; i < barList.size(); i++) {
            int modDaySlope = daysSlope;
            int modSecondSlope = secondSlopeDays;
            if (barList.get(i).getMarketCap() < 2000000000) {
                modDaySlope = modDaySlope;
            } else if (barList.get(i).getMarketCap() < 50000000000L) {
                modDaySlope = modDaySlope;
            } else if (barList.get(i).getMarketCap() > 50000000000L) {
                modDaySlope = modDaySlope * 2;
                modSecondSlope = (int) (modSecondSlope * (configurationTest.getMovingTrendLength()[5] * 0.01));
            }
            if (i < barList.size() - modSecondSlope) {
                SimpleRegression priceSlopeRegression = new SimpleRegression();
                SimpleRegression simpleRegression = new SimpleRegression();
                SimpleRegression treasuryRocRegression = new SimpleRegression();
                SimpleRegression dollarRocRegression = new SimpleRegression();
                SimpleRegression oilRocRegression = new SimpleRegression();
                SimpleRegression commodityRocRegression = new SimpleRegression();
                for (int x = 0; x <= modSecondSlope - 1; x++) {
                    if (isLong) {
                        simpleRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getSignalSlopeLong());
                        treasuryRocRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getTreasuryYieldSlope());
                        dollarRocRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getDollarSlope());
                        oilRocRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getOilSlope());
                        commodityRocRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getCommoditySlope());
                    } else {
                        //simpleRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getSignalSlopeShort());
                        treasuryRocRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getTreasuryYieldSlope());
                        dollarRocRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getDollarSlope());
                    }
                    if (isLong) {
                        priceSlopeRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getPriceSlope());
                    } else {
                        priceSlopeRegression.addData((modSecondSlope - x), barList.get(i + (modSecondSlope - x)).getPriceSlope());
                    }

                }
                barList.get(i).setTreasuryYieldSlopeRoc(treasuryRocRegression.getSlope());
                barList.get(i).setDollarSlopeRoc(dollarRocRegression.getSlope());
                barList.get(i).setCommodityRateOfChange(commodityRocRegression.getSlope());
                barList.get(i).setOilSlopeRoc(oilRocRegression.getSlope());
                if (isLong) {
                    barList.get(i).setPriceSlopeRoc(priceSlopeRegression.getSlope());
                    barList.get(i).setSignalRocLong(simpleRegression.getSlope());
                }
            }
        }
        }catch (Exception e){
            e.printStackTrace();
        }
//        for(int i = 0; i < barList.size(); i++) {
//            if(i < barList.size() - volatilitySlopeDays){
//                List<Double> vixDoubles = new ArrayList<>();
//                List<Long> dateLongs = new ArrayList<>();
//                for(int x = 1; x <= volatilitySlopeDays; x++){
//                    dateLongs.add(barList.get(i + x).getDate().getTime());
//                    vixDoubles.add(barList.get(i+x).getVixValue());
//                }
//                SimpleRegression volumeRegression = new SimpleRegression();
//                for(int z = 0; z <dateLongs.size(); z++){
//                    volumeRegression.addData(dateLongs.get(z),vixDoubles.get(z));
//                }
//                if(isLong){
//                    barList.get(i).setVixRoCLong(volumeRegression.getSlope());
//                }else {
//                    barList.get(i).setVixRoCShort(volumeRegression.getSlope());
//                }
//            }
//        }
        for(int i = 0; i < barList.size(); i++) {

            int modVolSlopeDays = volatilitySlopeDays;
            if(barList.get(i).getMarketCap() < 2000000000){
                modVolSlopeDays = modVolSlopeDays;
            }else if(barList.get(i).getMarketCap() < 50000000000L){
                modVolSlopeDays = modVolSlopeDays;
            }else if(barList.get(i).getMarketCap() > 50000000000L){
                modVolSlopeDays = (int) (modVolSlopeDays*(configurationTest.getMovingTrendLength()[5]*0.01));

            }
            if (i < barList.size() - modVolSlopeDays) {
                SimpleRegression volatilityRegression = new SimpleRegression();
                for (int x = 0; x <= modVolSlopeDays - 1; x++) {
                    if(barList.get(i + (modVolSlopeDays - x)).getBaseVolatility() != 0){
                        volatilityRegression.addData((modVolSlopeDays - x), barList.get(i + (modVolSlopeDays - x)).getBaseVolatility());
                    }
                }
                if(volatilityRegression.getN() == modVolSlopeDays) {
                    if (isLong) {
                        barList.get(i).setVolatilitySlopeLong(volatilityRegression.getSlope());
                    }
                }
            }
        }
        //ALTERNATIVELY TRYING DIFFERENT VARIABLE THAN VOLAILITITYSLOPEDAYS
        for(int i = 0; i < barList.size(); i++) {
            int modVolSlopeDays = volatilitySlopeDays;
            if(barList.get(i).getMarketCap() < 2000000000){
                modVolSlopeDays = modVolSlopeDays;
            }else if(barList.get(i).getMarketCap() < 50000000000L){
                modVolSlopeDays = modVolSlopeDays;
            }else if(barList.get(i).getMarketCap() > 50000000000L){
                modVolSlopeDays = (int) (modVolSlopeDays*(configurationTest.getMovingTrendLength()[5]*0.01));

            }
            if (i < barList.size() - modVolSlopeDays) {
                SimpleRegression volatilityRoCRegression = new SimpleRegression();
                for (int x = 0; x <= modVolSlopeDays - 1; x++) {
                    if(barList.get(i + (modVolSlopeDays - x)).getVolatilitySlopeLong() != 0) {
                        if (isLong) {
                            volatilityRoCRegression.addData((modVolSlopeDays - x), barList.get(i + (modVolSlopeDays - x)).getVolatilitySlopeLong());
                        } else {
                            volatilityRoCRegression.addData((modVolSlopeDays - x), barList.get(i + (modVolSlopeDays - x)).getVolatilitySlopeShort());
                        }
                    }
                }
                if(isLong) {
                    Double test = volatilityRoCRegression.getSlope();
                    barList.get(i).setVolatilitySlopeRoCLong(test);
                }
            }
        }
    }

    public double[] convertListToArray(List<Double> originalList){
        double[] newArray = new double[originalList.size()];
        int size = originalList.size();
        for(int i = 0; i < size; i++){
            newArray[i] = originalList.get(i);
        }
        return newArray;
    }

}
