package BackTest;

import java.util.Date;

public class TestKey {

    public int days;
    public Date startDate;
    public Date stopDate;

    public double start;
    public double end;
    public boolean isLong;

    public TestKey(int days, Date startDate, Date stopDate, boolean isLong, double start, double end) {
        this.days = days;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.isLong = isLong;
        this.start = start;
        this.end = end;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public boolean isLong() {
        return isLong;
    }

    public void setLong(boolean aLong) {
        isLong = aLong;
    }
}
